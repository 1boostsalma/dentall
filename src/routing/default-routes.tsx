import { IRoute } from '../interfaces/routing';
import SettingsPage from '../pages/settings/Settings';
import PatientsPage from '../pages/dashboards/patients/PatientsPage';
import UsersPage from '../pages/dashboards/users/UsersPage';

import UsersAppPage from '../pages/dashboards/users_app/UsersAppPage';
import DashboardPage from '../pages/dashboards/dashboard/Dashboard';
import CabinetsPage from '../pages/dashboards/cabinets/CabinetsPage'; 
import TypesSoinsPage from '../pages/dashboards/types_soins/TypesSoinsPage';
import OperationsPage from '../pages/dashboards/opérations/OperationsPage'; 
import AjoutOperationPage from '../pages/dashboards/opérations/AjoutOperationPage'; 
import RendezVousPage from '../pages/dashboards/rendez_vous/RendezVousPage'; 
import Calendrier from '../pages/dashboards/rendez_vous/Calendrier';
import EditOperationPage from '../pages/dashboards/opérations/EditOperationPage';
import PatientProfilePage from '../pages/dashboards/patients/PatientProfilePage';
import Payments from '../pages/dashboards/payments/PaymentsPage';

import AssurancesPage from '../pages/dashboards/assurance/AssurancesPage';
export const defaultRoutes: IRoute[] = [
  {
    path: 'settings',
    component: SettingsPage
  },
  
  {
    path: 'assurances',
    component: AssurancesPage
  },
  {
    path: 'users',
    component: UsersPage
  },
  {
    path: 'users_app',
    component: UsersAppPage
  },
  {
    path: 'dashboard',
    component: DashboardPage
  },
  
  {
    path: 'cabinets',
    component: CabinetsPage
  },
  {
    path: 'opérations',
    component: OperationsPage
  },{
    path: 'Ajout',
    component: AjoutOperationPage
  },
  {
    path: 'patients',
    component: PatientsPage
  },
  {
    path: 'calendrier',
    component: Calendrier
  },
  {
    path: 'rendez-vous',
    component: RendezVousPage
  },{
    path: 'patient-profile',
    component: PatientProfilePage
  },{
    path: 'edit',
    component: EditOperationPage
  },
  {
    path: 'payments',
    component: Payments
  },
  {
    path: 'TypesSoins',
    component: TypesSoinsPage
  }
];
