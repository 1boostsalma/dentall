export interface IPatient {
  id:string,
  cabinet:string,	
  datenaissance:string,
  assurance:string,
  sexe:string,
  cin:string,
  nom:string,	
  prenom:string,
  adresse:string,
  telephone:string,
 
}
export interface IAppointment {
    id?:number;
    datedebutrdv: string;
    datefinrdv: string;
    patient: string;
    status: string;
    description:string;
}
export interface IBilling {
  id: number;
  date: string;
  patient: string;
  charges: number;
 
}