export interface IRendezVous {
    id?:number;
    datedebutrdv: string;
    datefinrdv: string;
    patient: string;
    status: string;
    description:string;
  } 

