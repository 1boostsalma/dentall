export interface IOperation {
  id: string;
  patient:string;
  dent:string;
  typesoins: string;
  dateOperation:string;
  deleted:string;
  cabinet:string;
}

