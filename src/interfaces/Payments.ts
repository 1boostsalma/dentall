export interface IPayment {
    id:string,
    date: string;
    patient: string;
    charges: number;
  
}
  