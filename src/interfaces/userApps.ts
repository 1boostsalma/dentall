export interface IUserApp {
    id?: string;
    nom: string;
    prenom?: string;
    email: string;  
    password?: string;
  }