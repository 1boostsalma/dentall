import React, { ReactNode } from 'react';

import { Table,Input } from 'antd';

import { ColumnProps } from 'antd/es/table';
import { IAssurance } from '../../../interfaces/assurances';
import {SearchOutlined} from '@ant-design/icons';
const columns: ColumnProps<IAssurance>[] = [
  {
    key: 'abreviationAssurance',
    dataIndex: 'abreviationAssurance',
    title: 'Abriviation',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.abreviationAssurance==value;
     },
    sorter: (a, b) => (a.abreviationAssurance > b.abreviationAssurance ? 1 : -1),
    render: (abreviationAssurance) => <strong>{abreviationAssurance}</strong>
  },
  {
    key: 'libelleAssurance',
    dataIndex: 'libelleAssurance',
    title: 'Libelle',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.libelleAssurance==value;
     },
    sorter: (a, b) => (a.libelleAssurance > b.libelleAssurance ? 1 : -1),
    render: (libelleAssurance) => <strong>{libelleAssurance} </strong>
  },
  
  
  {}
]; 
 

 
type Props = {
  data: IAssurance[];
  actions?: (Assurance: IAssurance) => ReactNode;
};

const AssuranceTable = ({ data, actions }: Props) => {
  const actionColumn: ColumnProps<IAssurance> = {
    key: 'actions',
    title: 'Actions',
    render: actions
  };

  const displayedColumns = actions ? [...columns, actionColumn] : columns;

  return (
    <Table
      rowKey='number'
      dataSource={data}
      columns={displayedColumns}
      pagination={{ hideOnSinglePage: true }}
    />
  ); 
};

export default AssuranceTable;
