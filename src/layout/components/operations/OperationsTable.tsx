import React, { ReactNode } from 'react';

import { Table ,Input } from 'antd';

import { ColumnProps } from 'antd/es/table';
import { IOperation } from '../../../interfaces/operations';
import {SearchOutlined} from '@ant-design/icons';
const columns: ColumnProps<IOperation>[] = [

  {
    key: 'typesoins',
    dataIndex: 'typesoins',
    title: 'Type',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.typesoins==value;
     },
    sorter: (a, b) => (a.typesoins > b.typesoins ? 1 : -1),
    render: (typesoins) => <strong>{typesoins['type']}</strong>
  },{
    key: 'dent',
    dataIndex: 'dent',
    title: 'Dent',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.dent==value;
     },
    sorter: (a, b) => (a.dent > b.dent ? 1 : -1),
    render: (dent) => <strong>{dent}</strong>
  },{
    key: 'patient',
    dataIndex: 'patient',
    title: 'Patient',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.patient==value;
     },
    sorter: (a, b) => (a.patient > b.patient ? 1 : -1),
    render: (patient) => <strong>{patient['nom']} {patient['prenom']}</strong>
  },
  {}
];

type Props = {
  data: IOperation[];
  actions?: (Operation: IOperation) => ReactNode;
};

const TypesSoinsTable = ({ data, actions }: Props) => {
  const actionColumn: ColumnProps<IOperation> = {
    key: 'actions',
    title: 'Actions',
    render: actions
  };

  const displayedColumns = actions ? [...columns, actionColumn] : columns;

  return (
    <Table
      rowKey='number'
      dataSource={data}
      columns={displayedColumns}
      pagination={{ hideOnSinglePage: true }}
    />
  ); 
};

export default TypesSoinsTable;
