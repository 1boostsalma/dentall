import React, { ReactNode } from 'react';

import { Avatar, Table , Input} from 'antd';

import { ColumnProps } from 'antd/es/table';
import { IAppointment } from '../../../interfaces/patients';
import {SearchOutlined} from '@ant-design/icons';


const columns: ColumnProps<IAppointment>[] = [
  {
    key: 'patient',
    dataIndex: 'patient',
    title: 'Patient',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.patient==value;
     },
    sorter: (a, b) => (a.patient > b.patient ? 1 : -1),
    render: (patient) => <strong>{patient['nom']} {patient['prenom']} </strong>
  },
  {
    key: 'datedebutrdv',
    dataIndex: 'datedebutrdv',
    
    title: 'Date début visite',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.datedebutrdv==value;
     },
    sorter: (a, b) => (a.datedebutrdv > b.datedebutrdv ? 1 : -1),
    render: (datedebutrdv) => <strong>{datedebutrdv}</strong>
  },
  {
    key: 'datefinrdv',
    dataIndex: 'datefinrdv',
    title: 'Date fin visite',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.datefinrdv==value;
     },
    sorter: (a, b) => (a.datefinrdv > b.datefinrdv ? 1 : -1),
    render: (datefinrdv) => <strong>{datefinrdv}</strong>
  },
  {}
];

type Props = {
  data: IAppointment[];
  actions?: (appointment: IAppointment) => ReactNode;
};

const AppointmentsTable = ({ data, actions }: Props) => {
  const actionColumn: ColumnProps<IAppointment> = {
    key: 'actions',
    title: 'Actions',
    render: actions
  };

  const displayedColumns = actions ? [...columns, actionColumn] : columns;

  return (
    <Table
      rowKey='number'
      dataSource={data}
      columns={displayedColumns}
      pagination={{ hideOnSinglePage: true }}
    />
  );
};

export default AppointmentsTable;
