import React, { ReactNode } from 'react';

import { Table ,Input} from 'antd';

import { ColumnProps } from 'antd/es/table';
import { ITypesSoins } from '../../../interfaces/TypesSoins';
import {SearchOutlined} from '@ant-design/icons';
const columns: ColumnProps<ITypesSoins>[] = [

  {
    key: 'type',
    dataIndex: 'type',
    title: 'Type',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.type==value;
     },
    sorter: (a, b) => (a.type > b.type ? 1 : -1),
    render: (type) => <strong>{type}</strong>
  },
  {}
];

type Props = {
  data: ITypesSoins[];
  actions?: (TypesSoins: ITypesSoins) => ReactNode;
};

const TypesSoinsTable = ({ data, actions }: Props) => {
  const actionColumn: ColumnProps<ITypesSoins> = {
    key: 'actions',
    title: 'Actions',
    render: actions
  };

  const displayedColumns = actions ? [...columns, actionColumn] : columns;

  return (
    <Table
      rowKey='number'
      dataSource={data}
      columns={displayedColumns}
      pagination={{ hideOnSinglePage: true }}
    />
  ); 
};

export default TypesSoinsTable;
