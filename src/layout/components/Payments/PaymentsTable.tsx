import React, { ReactNode } from 'react';
import { Table,Input ,Typography} from 'antd';
import { ColumnProps } from 'antd/es/table';
import { IPayment } from '../../../interfaces/Payments';
import {SearchOutlined} from '@ant-design/icons';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
const { Text } = Typography;
const columns: ColumnProps<IPayment>[] = [
{
    key: 'date',
    dataIndex: 'date',
    title: 'Date',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.date==value;
     },
    sorter: (a, b) => (a.date > b.date ? 1 : -1),
    render: (date) => <strong>{date}</strong>
  },
  {
    key: 'patient',
    dataIndex: 'patient',
    title: 'Patient',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.patient==value;
     },
    sorter: (a, b) => (a.patient > b.patient ? 1 : -1),
    render: (patient) => <strong>{patient['nom']}{patient['prenom']}</strong>
  },
  {
    key: 'charges',
    dataIndex: 'charges',
    title: 'charges',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.charges==value;
     },
    sorter: (a, b) => (a.charges > b.charges ? 1 : -1),
    render: (charges) => <strong>{charges}</strong>

  },
  {}
];

type Props = {
  data: IPayment[];
  actions?: (Payment: IPayment) => ReactNode;
};

const PaymentsTable = ({ data, actions }: Props) => {
  const actionColumn: ColumnProps<IPayment> = {
    key: 'actions',
    title: 'Actions',
    render: actions
  };

  const displayedColumns = actions ? [...columns, actionColumn] : columns;

  return (
    <Table
      rowKey='number'
      dataSource={data}
      columns={displayedColumns}
      pagination={{ hideOnSinglePage: true }}
      bordered
      summary={pageData => {
        let totalBorrow = 0;
        

        pageData.forEach(({ charges }) => {
          totalBorrow += charges;
          
        });

        return (
          <>
            <Table.Summary.Row>
              <Table.Summary.Cell index={1}>Total</Table.Summary.Cell>
              <Table.Summary.Cell index={2} >
                <Text type="danger">{totalBorrow}</Text>
              </Table.Summary.Cell>
             
            </Table.Summary.Row>
          </>
        );
      }}
    />
  ); 
};

export default PaymentsTable;
