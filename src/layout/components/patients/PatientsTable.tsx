import React, { ReactNode } from 'react';
import { Table , Input , Button} from 'antd';
import { ColumnProps } from 'antd/es/table';
import { IPatient } from '../../../interfaces/patients';
import {SearchOutlined} from '@ant-design/icons';
//import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';

const columns: ColumnProps<IPatient>[] = [
{
    //dataField: 'cin',
    key: 'cin',
    dataIndex: 'cin',
    title: 'CIN',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    ></Input>
    
    
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.cin==value;
     },
    sorter: (a, b) => (a.cin > b.cin ? 1 : -1),
    render: (cin) => <strong>{cin}</strong>
   },
  {
    key: 'nom',
    dataIndex: 'nom',
    title: 'Nom',
      filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.nom==value;
     },
    sorter: (a, b) => (a.nom > b.nom ? 1 : -1),
    render: (nom) => <strong>{nom}</strong>
  },
  {
    key: 'prenom',
    dataIndex: 'prenom',
    title: 'Prenom',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.prenom==value;
     },
    sorter: (a, b) => (a.prenom > b.prenom ? 1 : -1),
    render: (prenom) => <strong>{prenom}</strong>
  },
 
  {
    key: 'telephone',
    dataIndex: 'telephone',
    title: 'Telephone',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.telephone==value;
     },
    render: (telephone) => (
      <span className='d-flex align-baseline nowrap' style={{ color: '#336cfb' }}>
        <span className='icofont icofont-ui-cell-phone mr-1' style={{ fontSize: 16 }} />
        {telephone}
      </span>
    )
  }, 
  {
    key: 'adresse',
    dataIndex: 'adresse',
    title: 'Adresse',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.adresse==value;
     },
    sorter: (a, b) => (a.adresse > b.adresse ? 1 : -1),
    render: (adresse) => <strong>{adresse}</strong>

  },
  {}
];

type Props = {
  data: IPatient[];
  actions?: (Patient: IPatient) => ReactNode;
};

const patientsTable = ({ data, actions }: Props) => {
  const actionColumn: ColumnProps<IPatient> = {
    key: 'actions',
    title: 'Actions',
    render: actions
  };

  const displayedColumns = actions ? [...columns, actionColumn] : columns;

  return (
    <Table
      rowKey='number'
      dataSource={data}
      columns={displayedColumns}
      pagination={{ hideOnSinglePage: true }}
     
    />
  ); 
};

export default patientsTable;
