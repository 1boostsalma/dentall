import React, { ReactNode } from 'react';

import { Table,Input } from 'antd';

import { ColumnProps } from 'antd/es/table';
import { IUser } from '../../../interfaces/users';
import {SearchOutlined} from '@ant-design/icons';
const columns: ColumnProps<IUser>[] = [

  {
    key: 'nom',
    dataIndex: 'nom',
    title: 'Nom',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.nom==value;
     },
    sorter: (a, b) => (a.nom > b.nom ? 1 : -1),
    render: (nom) => <strong>{nom}</strong>
  },
  {
    key: 'prenom',
    dataIndex: 'prenom',
    title: 'Prenom',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.prenom==value;
     },
    sorter: (a, b) => (a.prenom > b.prenom ? 1 : -1),
    render: (prenom) => <strong>{prenom}</strong>
  },
  {
    key: 'email',
    dataIndex: 'email',
    title: 'Email',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.email==value;
     },
    sorter: (a, b) => (a.email > b.email ? 1 : -1),
    render: (email) => (
      <span className='nowrap' style={{ color: '#336cfb' }}>
        <span className='icofont icofont-ui-email mr-1' style={{ fontSize: 16 }} />
        {email}
      </span>
    )
  }, 
  {
    key: 'telephone',
    dataIndex: 'telephone',
    title: 'Telephone',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.telephone==value;
     },
    render: (telephone) => (
      <span className='d-flex align-baseline nowrap' style={{ color: '#336cfb' }}>
        <span className='icofont icofont-ui-cell-phone mr-1' style={{ fontSize: 16 }} />
        {telephone}
      </span>
    )
  },
  {
    key: 'roles',
    dataIndex: 'roles',
    title: 'Roles',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.roles==value;
     },
    sorter: (a, b) => (a.roles > b.roles ? 1 : -1),
    render: (roles) => <strong>{roles}</strong>
  },

  {
    key: 'cabinet',
    dataIndex: 'cabinet',
    title: 'Cabinet',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.cabinet==value;
     },
    sorter: (a, b) => (a.cabinet > b.cabinet ? 1 : -1),
    render: (cabinet) => <strong>{cabinet['nom']}</strong>
  },
  {}
];

type Props = {
  data: IUser[];
  actions?: (User: IUser) => ReactNode;
};

const UsersTable = ({ data, actions }: Props) => {
  const actionColumn: ColumnProps<IUser> = {
    key: 'actions',
    title: 'Actions',
    render: actions
  };

  const displayedColumns = actions ? [...columns, actionColumn] : columns;

  return (
    <Table
      rowKey='number'
      dataSource={data}
      columns={displayedColumns}
      pagination={{ hideOnSinglePage: true }}
    />
  ); 
};

export default UsersTable;
