import React, { ReactNode } from 'react';

import {  Tag, Table ,Input} from 'antd';

import { ColumnProps } from 'antd/es/table';
import { IRendezVous } from '../../../interfaces/rendez_vous';
import {SearchOutlined} from '@ant-design/icons';
const columns: ColumnProps<IRendezVous>[] = [

  {
    key: 'patient',
    dataIndex: 'patient',
    title: 'Patient',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.patient==value;
     },
    sorter: (a, b) => (a.patient > b.patient ? 1 : -1),
    render: (patient) => <strong>{patient['nom']} {patient['prenom']} </strong>
  },
  {
    key: 'datedebutrdv',
    dataIndex: 'datedebutrdv',
    title: 'Date Debut rdv',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.datedebutrdv==value;
     },
    sorter: (a, b) => (a.datedebutrdv > b.datedebutrdv ? 1 : -1),
    render: (datedebutrdv) => <strong>{datedebutrdv}</strong>
  },
  {
    key: 'datefinrdv',
    dataIndex: 'datefinrdv',
    title: 'Date Fin rdv',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.datefinrdv==value;
     },
    sorter: (a, b) => (a.datefinrdv > b.datefinrdv ? 1 : -1),
    render: (datefinrdv) => <strong>{datefinrdv}</strong>
  },
  {
    key: 'status',
    dataIndex: 'status',
    title: 'Status',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.status==value;
     },
    sorter: (a, b) => (a.status > b.status ? 1 : -1),
    
    render: (status) => <Tag style={{ borderRadius: 20 }} color={
      status =='Fait'?'#b7ce63':   status =='En attente'?'#336CFB': status =='Approuvé'?'#cec759':  
      '#ed5564' }> <strong>{status}</strong></Tag>
  },
  {}
]; 
 

 
type Props = {
  data: IRendezVous[];
  actions?: (RendezVous: IRendezVous) => ReactNode;
};

const RendezVousTable = ({ data, actions }: Props) => {
  const actionColumn: ColumnProps<IRendezVous> = {
    key: 'actions',
    title: 'Actions',
    render: actions
  };

  const displayedColumns = actions ? [...columns, actionColumn] : columns;

  return (
    <Table
      rowKey='number'
      dataSource={data}
      columns={displayedColumns}
      pagination={{ hideOnSinglePage: true }}
    />
  ); 
};

export default RendezVousTable;
