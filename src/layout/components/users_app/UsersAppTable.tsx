import React, { ReactNode } from 'react';

import { Table,Input } from 'antd';

import { ColumnProps } from 'antd/es/table';
import { IUserApp } from '../../../interfaces/userApps';
import {SearchOutlined} from '@ant-design/icons';
const columns: ColumnProps<IUserApp>[] = [

  {
    key: 'nom',
    dataIndex: 'nom',
    title: 'Nom', 
     filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.nom==value;
     },
    sorter: (a, b) => (a.nom > b.nom ? 1 : -1),
    render: (nom) => <strong>{nom}</strong>
  },
  {
    key: 'prenom',
    dataIndex: 'prenom',
    title: 'Prenom',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.prenom==value;
     },
    sorter: (a, b) => (a.prenom > b.prenom ? 1 : -1),
    render: (prenom) => <strong>{prenom}</strong>
  },
  {
    key: 'email',
    dataIndex: 'email',
    title: 'Email',
    filterDropdown:({setSelectedKeys,selectedKeys,confirm,clearFilters})=>{
      return (

        <>
         <Input autoFocus
            placeholder="Rechercher"
            value={selectedKeys[0]}
            onChange={(e)=>{
     setSelectedKeys(e.target.value ? [e.target.value] : [])
     }}
             onPressEnter={()=>{
               confirm();
             }}
            onBlur={()=>{
              confirm();
            }}
           
    />  
    </>
      );
    },
     filterIcon:()=>{
     return <SearchOutlined/>;

     },
     onFilter:(value,record)=>{
       return record.email==value;
     },
    sorter: (a, b) => (a.email > b.email ? 1 : -1),
    render: (email) => (
      <span className='nowrap' style={{ color: '#336cfb' }}>
        <span className='icofont icofont-ui-email mr-1' style={{ fontSize: 16 }} />
        {email}
      </span>
    )
  }, {}
];

type Props = {
  data: IUserApp[];
  actions?: (UserApp: IUserApp) => ReactNode;
};

const UserAppsTable = ({ data, actions }: Props) => {
  const actionColumn: ColumnProps<IUserApp> = {
    key: 'actions',
    title: 'Actions',
    render: actions
  };

  const displayedColumns = actions ? [...columns, actionColumn] : columns;

  return (
    <Table
      rowKey='number'
      dataSource={data}
      columns={displayedColumns}
      pagination={{ hideOnSinglePage: true }}
    />
  ); 
};

export default UserAppsTable;
