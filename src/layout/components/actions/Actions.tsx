import React from 'react';

import './Actions.scss';


import SettingsDropdown from './SettingsDropdown';

const Actions = () => (
  <div className='actions'>
    

    <SettingsDropdown />
  </div>
);

export default Actions;
