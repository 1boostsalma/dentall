import { getToken,getCabin } from "./common";
import axios from 'axios';
let token=getToken();
let cabin=getCabin();
export var  result = axios.get(`https://127.0.0.1:8000/api/rendezvouses?deleted=false&cabinet=${cabin}`,{
    headers: { 
    'content-type': 'application/json',
    'Authorization': `Bearer ${token}`
  }}).then(res => {
  result=res.data['hydra:totalItems'];
    });
    export var  patient = axios.get(`https://127.0.0.1:8000/api/patients?deleted=false&cabinet=${cabin}`,{
        headers: { 
        'content-type': 'application/json',
        'Authorization': `Bearer ${token}`
      }}).then(res => {
    patient=res.data['hydra:totalItems'];
        });
   export var operations = axios.get(`https://127.0.0.1:8000/api/operations?deleted=false&cabinet=${cabin}`,{
        headers: { 
        'content-type': 'application/json',
        'Authorization': `Bearer ${token}`
      }}).then(res => {
    operations=res.data['hydra:totalItems'];
        });