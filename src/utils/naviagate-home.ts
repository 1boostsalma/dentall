import { history } from '../redux/store';

export function navigateHome() {
  history.push('/vertical/dashboard');
}

export function navigateHomeadmin() {
  history.push('/VerticalAdmin/users_app');
}
