export const getUser = () => {
  const userStr = sessionStorage.getItem('user');
  if (userStr) return JSON.parse(userStr);
  else return null;
}

// return token
export const getToken = () => {
  return sessionStorage.getItem('token') || null;
}
export const getCabin = () => {
  return sessionStorage.getItem('cabin');
}
//suppression token
export const removeUserSession = () => {
  sessionStorage.removeItem('token');
 sessionStorage.removeItem('user');
 sessionStorage.removeItem('cabin');
}
//refresh token
export const setUserSession = (token) => {
  sessionStorage.setItem('token', token);
 // sessionStorage.setItem('user', JSON.stringify(user));
}
 export const setserSession = (cabin,user) => {
  sessionStorage.setItem('cabin',cabin);
  sessionStorage.setItem('user', JSON.stringify(user));
}
export const setadminSession=(user)=>{
   sessionStorage.setItem('user', JSON.stringify(user));
}