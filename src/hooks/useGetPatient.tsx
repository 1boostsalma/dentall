import { useEffect, useState } from 'react';

import { IPatient } from '../interfaces/patients';
import axios from 'axios';
import { getToken } from '../utils/common';

let token = getToken();

async function getPatients() {

 
  const result = await axios.get('https://127.0.0.1:8000/api/patients',{
    headers: { 
    'content-type': 'application/json',
    'Authorization': `Bearer ${token}`
  }});
  return result.data['hydra:member'] as IPatient[];
}

export function useGetPatients(): IPatient[] {
  const [patients, setPatients] = useState<IPatient[]>([]);

  useEffect(() => {
    getPatients().then((data) => {
      setPatients(data);
    });
  }, []);

  return patients;
}

export function useGetPatient(name: string) {
  const [patient, setPatient] = useState<IPatient>(null);
  const patients = useGetPatients();

  useEffect(() => {
    if (patients.length === 0) return;
    const newPatient = patients.find((doc) => doc['@id'] === name);

    if (newPatient === undefined) return;

    setPatient(newPatient);
  }, [name, patients]);

  return { patient };
}
