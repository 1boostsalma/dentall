import { useEffect, useState } from 'react';

import { IOperation } from '../interfaces/operations';
import axios from 'axios';
import { getToken } from '../utils/common';

let token = getToken();

async function getOperations() {

 
  const result = await axios.get('https://127.0.0.1:8000/api/operations',{
    headers: { 
    'content-type': 'application/json',
    'Authorization': `Bearer ${token}`
  }});
  return result.data['hydra:member'] as IOperation[];
 
}

export function useGetOperations(): IOperation[] {
  const [operations, setOperations] = useState<IOperation[]>([]);

  useEffect(() => {
    getOperations().then((data) => {
      setOperations(data);
    });
  }, []);

  return operations;

  console.log(operations);
}

export function useGetOperation(name: string) {
  const [operation, setOperation] = useState<IOperation>(null);
  const operations = useGetOperations();

  useEffect(() => {
    if (operations.length === 0) return;
    const newOperation = operations.find((doc) => doc['@id'] === name);

    if (newOperation === undefined) return;

    setOperation(newOperation);
  }, [name, operations]);

  return { operation };
  console.log('oz',operation);
}
