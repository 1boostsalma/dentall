import { useEffect, useState } from 'react';
import { useLocation } from "react-router-dom";
import { IBilling } from '../interfaces/patients';
import axios from 'axios';
import { patients} from '../pages/dashboards/patients/PatientProfilePage';



const pat=patients;

async function getBillings() {
  
  const result = await axios.get(`https://127.0.0.1:8000/api/paiements?patient=${patients}`);
  return result.data['hydra:member'] as IBilling[];
}

async function getPayments() {
  const result = await axios.get(`https://127.0.0.1:8000/api/paiements?patient=${patients}`);
  return result.data['hydra:member']  as IBilling[];
}

export function useGetBillings(): IBilling[] {
  const [billings, setBillings] = useState<IBilling[]>([]);

  useEffect(() => {
    getBillings().then((data) => {
      setBillings(data);
    });
  }, []);

  return billings;
}

export function useGetPayments(): [IBilling[], React.Dispatch<any>] {
  const [payments, setPayments] = useState<IBilling[]>([]);

  useEffect(() => {
    getPayments().then((data) => {
      setPayments(data);
    });
  }, []);

  return [payments, setPayments];
}
