import React,{useEffect} from 'react';

import { Switch, Route, Redirect } from 'react-router-dom';

import VerticalLayout from './layout/vertical/Vertical';
import VerticalAdminLayout from './layout/vertical/VerticalAdmin';
import {getToken} from './utils/common';
import NotFound from './pages/sessions/404';
import { defaultRoutes, sessionRoutes } from './routing';
import SignIn from './pages/sessions/Sign-in';
import SignInAdmin from './pages/sessions/Sign-in-admin';
import Logout  from './pages/sessions/logout';
import './App.scss';
import { useHideLoader } from './hooks/useHideLoader';

const Routes = ({ routes, layout = '' }) => (
  <Switch>
    {routes.map((route, index) => (
      <Route
        key={index}
        exact={route.exact}
        path={layout.length > 0 ? `/${layout}/${route.path}` : `/${route.path}\``}
        component={() => <route.component />}
      />
    ))}

    <Route path='*'>
      <Redirect to='/public/page-404' />
    </Route>
  </Switch>
);

const DefaultRoutes = ({ layout }) => getToken() ? <Routes routes={defaultRoutes} layout={layout} /> :<Routes routes={sessionRoutes} layout='public'/>;

const SessionRoutes = () =>  <Routes routes={sessionRoutes} layout='public' />;

const token = getToken();

const App = () => {
  useHideLoader();
 

  return (

    <Switch>
     
      
      <Route path='/public'>
        <SessionRoutes />
      </Route>
      
      <Route path='/' exact>
      <SignIn/>
      </Route>
      <Route path='/sign-in-admin' exact>
        <SignInAdmin/>
      </Route>
     
      <Route path='/vertical'>
        <VerticalLayout>
          <DefaultRoutes layout='vertical' />
        </VerticalLayout>
      </Route>
      <Route path='/VerticalAdmin'>
        <VerticalAdminLayout>
          <DefaultRoutes layout='VerticalAdmin' />
        </VerticalAdminLayout>
      </Route>
     
      <Route path='*'>
        <NotFound />
      </Route>
    </Switch>
  );

};

export default App;
