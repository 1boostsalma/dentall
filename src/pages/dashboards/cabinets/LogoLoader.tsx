import React, { ChangeEvent, useEffect, useRef, useState } from 'react';

import { Avatar, Button } from 'antd';

const anonymouslogo = `${window.origin}/content/user-40-1.jpg`;

type Props = {
  src?: string;
  size?: number;
  onLoad?: (logo: any) => void;
};

const LogoLoader = ({ src, size = 40, onLoad = () => null }: Props) => {
  const input = useRef<HTMLInputElement>(null);
  const [logo, setLogo] = useState<string>(anonymouslogo);

  useEffect(() => {
    if (!src) {
      onLoad(logo);
    }

    return () => {
      setLogo(null);
    };
  }, []);

  const handleClick = () => input.current.click();

  const handleLoad = (e: ChangeEvent<HTMLInputElement>) => {
    let file: File = e.target.files[0];
    let reader: FileReader = new FileReader();

    reader.onloadend = () => {
      const result = reader.result as string;

      onLoad(result);
      setLogo(result);
    };

    reader.readAsDataURL(file);
  };

  const icon = <span className='icofont icofont-ui-image' />;

  return (
    <>
      <input ref={input} onChange={handleLoad} type='file' style={{ display: 'none' }} />
      <div className='d-flex align-items-center'>
        <Avatar src={src || logo} size={size} className='mr-4' />

        <Button type={'primary'} className='btn-outline' onClick={handleClick}>
          Select {icon}
        </Button>
      </div>
    </>
  );
};

export default LogoLoader;
