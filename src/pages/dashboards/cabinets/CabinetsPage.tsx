import React, { useState } from 'react';

import { Button } from 'antd';
import { getToken } from '../../../utils/common'
import EditCabinet from './EditCabinet';
import AddCabinet from './AddCabinet';
import CabinetsTable from '../../../layout/components/cabinets/CabinetsTable';
import axios from'axios'
import PageAction from '../../../layout/components/page-action/PageAction';
import { useFetchPageData, usePageData } from '../../../hooks/usePage';

import { ICabinet } from '../../../interfaces/cabinets';
import { IPageData } from '../../../interfaces/page';

const pageData: IPageData = {
  title: 'Cabinets',
  fulFilled: false,
  breadcrumbs: [
    {
      title: 'Dashboard',
      route: 'default-dashboard'
    },
    {
      title: 'Cabinets'
    }
  ]
};

const CabinetsPage = () => {
  usePageData(pageData);
  const [Cabinets, setCabinets] = useFetchPageData<ICabinet[]>(
    'https://127.0.0.1:8000/api/cabinets?deleted=false',
    []
  );

  const [selectedCabinet, setSelectedCabinet] = useState(null);
  const [addingModalVisibility, setAddingModalVisibility] = useState(false);
  const token = getToken();
  const handleDelete = (Cabinet: ICabinet) => {
    axios.put(`https://127.0.0.1:8000/api/cabinets/${Cabinet.id}`,{
      "deleted": true
    },{
    headers: { 
    'content-type': 'application/json',
    'Authorization': `Bearer ${token}`
  }});
  const newCabinets = Cabinets.filter((el) => el !== Cabinet);
  setCabinets(newCabinets);
  };

  const handleEdit = (Cabinet: ICabinet) => {
    const editedCabinets = Cabinets.map((el) =>
      el !== selectedCabinet ? el : Cabinet
    );
    setCabinets(editedCabinets);
    setSelectedCabinet(null);
  };

  const openAddingModal = () => setAddingModalVisibility(true);

  const addCabinet = (Cabinet: ICabinet) => {
    setCabinets([Cabinet, ...Cabinets]);
    setAddingModalVisibility(false);
  };

  const closeAddingModal = () => setAddingModalVisibility(false);

  const openEditModal = (Cabinet: ICabinet) => setSelectedCabinet(Cabinet);

  const closeModal = () => {
    setSelectedCabinet(null);
  };

  const CabinetsActions = (Cabinet: ICabinet) => (
    <div className='buttons-list nowrap'>
      <Button onClick={openEditModal.bind({}, Cabinet)} shape='circle' type='primary'>
        <span className='icofont icofont-edit-alt' />
      </Button>
      <Button onClick={handleDelete.bind({}, Cabinet)} shape='circle' danger>
        <span className='icofont icofont-ui-delete' />
      </Button>
    </div>
  );

  return (
    <>
      <CabinetsTable data={Cabinets} actions={CabinetsActions} />

      <PageAction onClick={openAddingModal} icon='icofont-plus' type={'primary'} />

      <AddCabinet
        onClose={closeAddingModal}
        visible={addingModalVisibility}
        onSubmit={addCabinet}
      />

      <EditCabinet
        Cabinet={selectedCabinet}
        visible={!!selectedCabinet}
        onClose={closeModal}
        onEdited={handleEdit}
      />
    </>
  );
};

export default CabinetsPage;
