import React, { useEffect, useState } from 'react';

import { Button, Input } from 'antd';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import LogoLoader from './LogoLoader';
import { hasErrorFactory } from '../../../utils/hasError';
import { getToken } from '../../../utils/common';
import { ICabinet } from '../../../interfaces/cabinets';

type Props = {
  cabinet?: ICabinet;
  onSubmit: (cabinet: ICabinet) => void; 
  onCancel: () => void;
  submitText?: string;
};

const defaultSubmitText = 'Ajouter';

const emptyCabinet = {
  nom: '',
  logo: '',
  email: '',
  adresse: '',
  telephone: '',
};
const token = getToken();
const cabinetSchema = Yup.object().shape({
  nom: Yup.string().required(),
  logo: Yup.string().required(),
  email: Yup.string().required(),
  adresse: Yup.string().required(),
  telephone: Yup.string().required()
});

function refreshPage(){ 
  window.location.reload(); 
}

const CabinetForm = ({
  submitText = defaultSubmitText,
  cabinet = emptyCabinet,
  onSubmit,
  onCancel
}: Props) => {
  const {
    handleSubmit,
    handleChange,
    handleBlur,
    values,
    setValues,
    isValid,
    errors,
    touched,
    resetForm
  } = useFormik<ICabinet>({
    validationSchema: cabinetSchema,
    initialValues: cabinet,

    onSubmit: (form) => {
      if(submitText == 'Ajouter')
    {
      axios.post('https://127.0.0.1:8000/api/cabinets',{
      "nom":values.nom,
      "logo": values.logo,
      "email": values.email,
      "telephone": values.telephone,
      "adresse": values.adresse,
      "deleted":false
    },
      {
        headers: { 
        'content-type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
      })    
      onSubmit({ ...form });
      resetForm();
   }

    if(submitText == 'Modifier')
    {
    axios.put(`https://127.0.0.1:8000/api/cabinets/${cabinet.id}`,{
      "nom":values.nom,
      "logo": values.logo,
      "email": values.email,
      "telephone": values.telephone,
      "adresse": values.adresse,
      "deleted":false
     },{
      headers: { 
      'content-type': 'application/json',
      'Authorization': `Bearer ${token}`
    }})

   onSubmit({ ...form });
     resetForm();
  }

}
});  

  const [logo, setLogo] = useState(values.logo);
 // const [status, setStatus] = useState('');
  useEffect(() => {
    setValues({ ...values });
  }, [cabinet]);

  const handleImageLoad = (logo) => {
    setValues({ ...values, logo});

  };

  const handleCancel = () => {
    resetForm();
    onCancel();
  };

  const hasError = hasErrorFactory(touched, errors);
  
  
  return (
    <>
      <form onSubmit={handleSubmit}>
        <div className='form-group'>
          <LogoLoader onLoad={handleImageLoad} src={values.logo} />
        </div>
        
        <div className='form-group'>
          <Input
            name='nom'
            placeholder='Nom'
            onBlur={handleBlur}
            onChange={handleChange}
            defaultValue={values.nom}
            className={hasError('nom')}
          />
        </div>

        <div className='form-group'>
          <Input
            defaultValue={values.adresse}
            placeholder='Adresse'
            onBlur={handleBlur}
            name='adresse'
            onChange={handleChange}
            className={hasError('adresse')}
          />
        </div>

        <div className='form-group'>
          <Input
            defaultValue={values.email}
            placeholder='Email'
            name='email'
            type='email'
            onBlur={handleBlur}
            onChange={handleChange}
            className={hasError('email')}
          />
        </div>

        <div className='form-group'>
          <Input
            type='telephone'
            name='telephone'
            onBlur={handleBlur}
            placeholder='Telephone'
            onChange={handleChange}
            defaultValue={values.telephone}
            className={hasError('telephone')}
          />
        </div>

        <div className='d-flex justify-content-between buttons-list settings-actions'>
          <Button danger onClick={handleCancel}>
            Annuler
          </Button>
          <Button disabled={!isValid} type='primary' htmlType='submit'>
            {submitText}
          </Button>
        </div>
      </form>
    </>
  );
};

export default CabinetForm;
