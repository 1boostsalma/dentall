import React from 'react';

import { Modal } from 'antd';
//import axios from 'axios';
import CabinetForm from './CabinetForm';
import { ICabinet } from '../../../interfaces/cabinets';

type Props = {
  onSubmit: (Cabinet: ICabinet) => void;
  visible: boolean;
  onClose: () => void;
};

const AddCabinet = ({ visible, onClose, onSubmit }: Props) => {

  return (
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Ajouter</h3>}
    >
      <CabinetForm onCancel={onClose} onSubmit={onSubmit} submitText='Ajouter' />
    </Modal>
  );
};

export default AddCabinet;
