import React from 'react';
import { ICabinet } from '../../../interfaces/cabinets';
import CabinetForm from './CabinetForm';
import { Modal } from 'antd';

type Props = {
  onEdited: (ICabinet) => void;
  Cabinet: ICabinet;
  visible: boolean;
  onClose: () => void;
};
//const token = getToken();


const EditCabinet = ({ Cabinet,visible, onClose, onEdited }: Props) => {
   
  return (
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Modifier</h3>}
    >
      <CabinetForm
        onCancel={onClose}
        onSubmit={onEdited}
        cabinet={Cabinet}
        submitText='Modifier'
      />
    </Modal>
  );
};
export default EditCabinet;
