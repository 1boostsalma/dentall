import React from 'react';
import { getToken } from '../../../utils/common';
import { Modal } from 'antd';

import PaymentForm from './PaymentForm';
import { IPayment } from '../../../interfaces/Payments';
type Props = {
  onSubmit: (Payment: IPayment) => void;
  visible: boolean;
  onClose: () => void;
};

const AddPayment = ({ visible, onClose, onSubmit }: Props) => {
 const token = getToken();
 
  return (
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Ajouter</h3>}
    >
      <PaymentForm onCancel={onClose} onSubmit={onSubmit} submitText='Ajouter' />
    </Modal>
  );
};

export default AddPayment;
