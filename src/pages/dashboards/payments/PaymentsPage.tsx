import React, { useState } from 'react';
import { Button } from 'antd';
import EditPayment from './EditPayment';
import AddPayment from './AddPayment';
import PaymentsTable from '../../../layout/components/Payments/PaymentsTable';
import PageAction from '../../../layout/components/page-action/PageAction';
import { useFetchPageData, usePageData } from '../../../hooks/usePage';
import { IPayment } from '../../../interfaces/Payments';
import { IPageData } from '../../../interfaces/page';
import {getCabin} from '../../../utils/common';
import axios from 'axios';
//import { history } from '../../../redux/store';
import { getToken } from '../../../utils/common';

const pageData: IPageData = {
  title: 'Payments',
  fulFilled: true,
  breadcrumbs: [
    {
      title: 'dashboard',
      route: 'default-dashboard'
    },
    {
      title: 'Payments'
    }
  ]
};
const cabin=getCabin();

const PaymentsPage = () => {
  usePageData(pageData);
  const [Payments, setPayments] = 

  useFetchPageData<IPayment[]>(
    `https://127.0.0.1:8000/api/paiements?deleted=false&cabinet=${cabin}`,
    []
    ); 
  
  const [selectedPayment, setSelectedPayment] = 

useState(null);
  const [addingModalVisibility, 

setAddingModalVisibility] = useState(false);
  const handleDelete = (Payment: IPayment) => {
    let token = getToken();
    axios.put(`https://127.0.0.1:8000/api/paiements/${Payment.id}`,{
      "deleted": true
    },{
    headers: { 
    'content-type': 'application/json',
    'Authorization': `Bearer ${token}`
  }});
    const newPayments = Payments.filter((el) => el !== 

Payment);
    setPayments(newPayments);
  };
  
  const handleEdit = (Payment: IPayment) => {
    const editedPayments = Payments.map((el) =>
      el !== selectedPayment ? el : Payment
    );
    setPayments(editedPayments);
    setSelectedPayment(null);
  };
  const openAddingModal = () => 

setAddingModalVisibility(true);
  const addPayment = (Payment: IPayment) => {
    setPayments([Payment, ...Payments]);
    setAddingModalVisibility(false);
  };
  const closeAddingModal = () => 

setAddingModalVisibility(false);
  const openEditModal = (Payment: IPayment) => 

setSelectedPayment(Payment);
  const closeModal = () => {
    setSelectedPayment(null);
  };


  


  const PaymentsActions = (Payment: IPayment) => (

    <div className='buttons-list nowrap'>
       
      
      <Button onClick={openEditModal.bind({},Payment)} shape='circle' type='primary'>
        <span className='icofont icofont-edit-alt' />
      </Button>
      <Button onClick={handleDelete.bind({}, Payment)} shape='circle' danger>
        <span className='icofont icofont-ui-delete' 

/>
      </Button>

    </div>
  );
  
  return (
    <>
      <PageAction onClick={openAddingModal} 

icon='icofont-plus' type={'primary'} />
      <PaymentsTable data={Payments} actions=

{PaymentsActions} />
      <AddPayment
        onClose={closeAddingModal}
        visible={addingModalVisibility} 
        onSubmit={addPayment}
      />
      <EditPayment
        Payment={selectedPayment}
        visible={!!selectedPayment}
        onClose={closeModal}
        onEdited={handleEdit}
      />
      
    </>
  );
};
export default PaymentsPage;