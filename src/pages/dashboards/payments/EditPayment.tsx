import React from 'react';
import { IPayment } from '../../../interfaces/Payments';
import PaymentForm from './PaymentForm';
import { Modal } from 'antd';

type Props = {
  onEdited: (IPayment) => void;
  Payment: IPayment;
  visible: boolean;
  onClose: () => void;
};

const EditPayment = ({ Payment, visible, onClose, onEdited }: Props) => {
  return (
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Modifier Payment</h3>}
    >
      <PaymentForm
        onCancel={onClose}
        onSubmit={onEdited}
        Payment={Payment}
        submitText='Modifier'
      />
    </Modal>
  );
};

export default EditPayment;
