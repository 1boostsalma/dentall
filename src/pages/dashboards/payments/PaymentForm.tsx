import React, { useEffect } from 'react';
import { Button, Select, Input } from 'antd';
import { useFetch } from '../../../hooks/useFetch';
import {getCabin} from '../../../utils/common';
import { getToken } from '../../../utils/common';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import { hasErrorFactory } from '../../../utils/hasError';
import { IPayment } from '../../../interfaces/Payments';
//import { AnyARecord } from 'dns';
type Props = {
  Payment?: IPayment;
  onSubmit: (Payment: IPayment) => void;
  onCancel: () => void;
  submitText?: string;
};


const defaultSubmitText = 'Ajouter';
const emptyPayment = {
  
  id:'',
    date: '',
    patient:'',
    charges:null,

  deleted:'false'
};
const token = getToken();
const cabin=getCabin();

const PaymentSchema = Yup.object().shape({
  
  date: Yup.string().required(),
  patient: Yup.string().required(),
  charges: Yup.number().required()

});
function refreshPage(){ 
  window.location.reload(); 
}
const PaymentForm = ({
  submitText = defaultSubmitText,
  Payment = emptyPayment,
  onSubmit,
  onCancel
}: Props) => {
  const [patients] = useFetch<{ value: any }[]>('https://127.0.0.1:8000/api/patients?deleted=false', []);
  const cabin=getCabin();
  const {
    handleSubmit ,
    handleChange,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    values,
    setValues,
    isValid,
    errors,
    touched,
    resetForm
  } = useFormik<IPayment>({
    validationSchema: PaymentSchema,
    initialValues: Payment,
    
    onSubmit: (form) =>{
      if(submitText=='Ajouter')
    {
      axios.post('https://127.0.0.1:8000/api/paiements',{
     
     "patient":values.patient,
    "cabinet":cabin,
     "date":values.date,
     "charges":values.charges,
     "deleted":false
    },
      {
        headers: { 
        'content-type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
      })    
      onSubmit({ ...form });
      resetForm();
 //refreshPage();
   }
   else  if(submitText=='Modifier')
    {
    axios.put(`https://127.0.0.1:8000/api/paiements/${Payment.id}`,{
      "patient":values.patient,
     "cabinet":cabin,
      "date":values.date,
      "charges":values.charges,
      "deleted":false
     },{
      headers: { 
      'content-type': 'application/json',
      'Authorization': `Bearer ${token}`
    }})
   onSubmit({ ...form });
      resetForm();
  }
}});
    
  useEffect(() => {
    setValues({ ...values });
  }, [Payment]);
  
 const handlepatientSelect = (patient) => {
    setValues({ ...values,  patient });
  };


  const handleCancel = () => {
    resetForm();
    onCancel();
  };
  
  const hasError = hasErrorFactory(touched, errors);

  return (
    <>
      <form onSubmit={handleSubmit}>
      
         

        
        
<div className='row'>
           <div className='col-sm-6 col-12'>
           <div className='form-group'>
          <Input
          type='number'
            defaultValue={values.charges}
            placeholder='charges'
            onBlur={handleBlur}
            name='charges'
            onChange={handleChange}
            className={hasError('charges')}
          />
        </div>     
  </div>
          <div className='col-sm-6 col-12'>
           <div className='form-group'>
          <Input
            type="date"
            defaultValue={values.date}
            placeholder='date'
            name='date'
            onChange={handleChange}
            onBlur={handleBlur}
            className={hasError('date')}
          />
        </div>
         </div>
         </div>
         <div className='row'>
              <div className='col-sm-6 col-12'>
              <div className='form-group'>
          <Select
            placeholder='patient'
            defaultValue={values.patient['@id']}
            onChange={handlepatientSelect}
            className={hasError('patient')}
            onBlur={() => setFieldTouched('patient')}
          >
            {               
              patients.map((patient)=>(
            <Select.Option value={patient['@id']}>{patient['nom']} {patient['prenom']}</Select.Option>
              ))
            }
          </Select>
        </div>
        </div>
          
        
        </div>
        <div className='d-flex justify-content-between buttons-list settings-actions'>
          <Button danger onClick={handleCancel}>
            Annuler
          </Button>
          <Button disabled={!isValid} type='primary' htmlType='submit'>
            {submitText}
          </Button>
       
         </div>
      </form>
    </>
  );
};
export default PaymentForm;