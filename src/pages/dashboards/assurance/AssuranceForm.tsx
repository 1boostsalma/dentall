import React, { useEffect } from 'react';

import { Button, Input } from 'antd';

import { useFetch } from '../../../hooks/useFetch';

import { getToken ,getCabin} from '../../../utils/common';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import { hasErrorFactory } from '../../../utils/hasError';
import { Alert } from 'antd';
import { IAssurance } from '../../../interfaces/assurances';


type Props = {
  Assurance?: IAssurance;
  onSubmit: (Assurance: IAssurance) => void;
  onCancel: () => void;
  submitText?: string;
};

const defaultSubmitText = 'Ajouter';
const emptyAssurance = {
  libelleAssurance: '',
    
  abreviationAssurance: ''
};

const token = getToken();
const AssuranceSchema = Yup.object().shape({
  libelleAssurance: Yup.string().required(),
  abreviationAssurance: Yup.string().required()
  
});
let cabin=getCabin();
function refreshPage()
{ 
  window.location.reload(); 
}
const AssuranceForm = ({
  submitText = defaultSubmitText,
  Assurance = emptyAssurance,
  onSubmit,
  onCancel
}: Props) => {

  const {
    handleSubmit ,
    handleChange,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    values,
    setValues,
    isValid,
    errors,
    touched,
    resetForm
  } = useFormik<IAssurance>({
    validationSchema: AssuranceSchema,
    initialValues: Assurance,
    onSubmit: (form) =>{
      if(submitText=='Ajouter')
    {
      axios.post('https://127.0.0.1:8000/api/assurances',{
    
     "libelleAssurance":values.libelleAssurance,
     "abreviationAssurance": values.abreviationAssurance,
      "deleted":false
    },
      {
        headers: { 
        'content-type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
      }).then(status=> {
      if (status.status === 200 ||status.status === 201)
         // setStatus("Ajout success");
          refreshPage();
   
      }).catch(error => {        
       if (error.response.status)
        
       refreshPage();
      });     
      onSubmit({ ...form });
      resetForm();
   }
   else  if(submitText=='Modifier')
    {
    axios.put(`https://127.0.0.1:8000/api/assurances/${Assurance.id}`,{
      "libelleAssurance":values.libelleAssurance,
     "abreviationAssurance": values.abreviationAssurance,
      "deleted":false
     },{
      headers: { 
      'content-type': 'application/json',
      'Authorization': `Bearer ${token}`
    }}).then(status=> {
     
   if (status.status === 200 ||status.status === 201)
      
        refreshPage();
 
    }).catch(error => {        
      if (error.response.status)
     {}
     refreshPage();
    });  
    onSubmit({ ...form });
    resetForm();
    }
}});
     
     
  useEffect(() => {
    setValues({ ...values });
  }, []);

  const handleCancel = () => {
    resetForm();
    onCancel();
  };
 
  const hasError = hasErrorFactory(touched, errors);

  return (
    <>
      <form onSubmit={handleSubmit}>
     
      <div className='form-group'>
          <Input
            defaultValue={values.abreviationAssurance}
            placeholder='Abriviation'
            onBlur={handleBlur}
            name='abreviationAssurance'
            onChange={handleChange}
            className={hasError('abreviationAssurance')}
          />
        </div>
   

        <div className='form-group'>
          <Input
            name='libelleAssurance'
            placeholder='Libelle'
            onBlur={handleBlur}
            onChange={handleChange}
            defaultValue={values.libelleAssurance}
            className={hasError('libelleAssurance')}
          />
        </div>

       

        
        <div className='d-flex justify-content-between buttons-list settings-actions'>
          <Button danger onClick={handleCancel}>
              Annuler
          </Button>
          <Button disabled={!isValid} type='primary' htmlType='submit'>
            {submitText}
          </Button>
        </div>
       
      </form>
    </>
  );
};

export default AssuranceForm;
