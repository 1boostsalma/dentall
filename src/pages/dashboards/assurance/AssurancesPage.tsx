import React, { useState } from 'react';
import { Button } from 'antd';
import EditAssurance from './EditAssurance';
import AddAssurance from './AddAssurance';
import AssurancesTable from '../../../layout/components/assurances/AssurancesTable';
import PageAction from '../../../layout/components/page-action/PageAction';
import { useFetchPageData, usePageData } from '../../../hooks/usePage';
import { IAssurance } from '../../../interfaces/assurances';
import { IPageData } from '../../../interfaces/page';
import axios from 'axios';
import { getToken ,getCabin} from '../../../utils/common';

const pageData: IPageData = {
  title: 'Assurances',
  fulFilled: false,
  breadcrumbs: [
    {
      title: 'Dashboard',
      route: 'default-dashboard'
    },
    {
      title: 'Assurances'
    }
  ]
};
const AssurancesPage = () => {
  usePageData(pageData);
  let cabin=getCabin();
  const [Assurances, setAssurances] = useFetchPageData<IAssurance[]>(
    `https://127.0.0.1:8000/api/assurances?deleted=false&cabinet=${cabin}`,
    []
    );   
  const [selectedAssurance, setSelectedAssurance] = useState(null);
  const [addingModalVisibility, setAddingModalVisibility] = useState(false);
  const handleDelete = (Assurance: IAssurance) => {
    let token = getToken();
    axios.put(`https://127.0.0.1:8000/api/assurances/${Assurance.id}`,{
      "deleted": true
    },{
    headers: { 
    'content-type': 'application/json',
    'Authorization': `Bearer ${token}`
  }});
    const newAssurances = Assurances.filter((el) => el !== Assurance);
    setAssurances(newAssurances);
  };
  const handleEdit = (Assurance: IAssurance) => {
    const editedAssurances = Assurances.map((el) =>
      el !== selectedAssurance ? el : Assurance
    );
    setAssurances(editedAssurances);
    setSelectedAssurance(null);
  };
  const openAddingModal = () => setAddingModalVisibility(true);
  const addAssurance = (Assurance: IAssurance) => {
    setAssurances([Assurance, ...Assurances]);
    setAddingModalVisibility(false);
  };
  const closeAddingModal = () => setAddingModalVisibility(false);
  const openEditModal = (Assurance: IAssurance) => setSelectedAssurance(Assurance);
  const closeModal = () => {
    setSelectedAssurance(null);
  };
  const AssurancesActions = (Assurance: IAssurance) => (
    <div className='buttons-list nowrap'>
      <Button onClick={openEditModal.bind({}, Assurance)} shape='circle' type='primary'>
        <span className='icofont icofont-edit-alt' />
      </Button>
      <Button onClick={handleDelete.bind({}, Assurance)} shape='circle' danger>
        <span className='icofont icofont-ui-delete' />
      </Button>
    </div>
  );
  return (
    <> 
      
       <AssurancesTable data={Assurances} actions={AssurancesActions} />
      <PageAction onClick={openAddingModal} icon='icofont-plus' type={'primary'} />
     
      <AddAssurance
        onClose={closeAddingModal}
        visible={addingModalVisibility} 
        onSubmit={addAssurance}
      />
      <EditAssurance
        Assurance={selectedAssurance}
        visible={!!selectedAssurance}
        onClose={closeModal}
        onEdited={handleEdit}
      />
    </>
  );
};
export default AssurancesPage;
