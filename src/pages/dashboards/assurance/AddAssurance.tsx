import React from 'react';
//import { getToken } from '../../../utils/common';
import { Modal } from 'antd';

import AssuranceForm from './AssuranceForm';
import { IAssurance } from '../../../interfaces/assurances';
type Props = {
  onSubmit: (Assurance: IAssurance) => void;
  visible: boolean;
  onClose: () => void;
};

const AddAssurance = ({ visible, onClose, onSubmit }: Props) => {

 
  return (
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Ajouter</h3>}
    >
      <AssuranceForm onCancel={onClose} onSubmit={onSubmit} submitText='Ajouter' />
    </Modal>
  );
};

export default AddAssurance;
