import React from 'react';
import { IAssurance } from '../../../interfaces/assurances';
import AssuranceForm from './AssuranceForm';
import { Modal } from 'antd';

type Props = {
  onEdited: (IAssurance) => void;
  Assurance: IAssurance;
  visible: boolean;
  onClose: () => void;
};

const EditAssurance = ({ Assurance, visible, onClose, onEdited }: Props) => {
  return (
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Modifier Assurance</h3>}
    >
      <AssuranceForm
        onCancel={onClose}
        onSubmit={onEdited}
        Assurance={Assurance}
        submitText='Modifier'
      />
    </Modal>
  );
};

export default EditAssurance;
