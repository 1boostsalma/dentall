import React from 'react';
import { result } from '../../../utils/dashboard';
import { patient } from '../../../utils/dashboard';
import { operations } from '../../../utils/dashboard';
import { Card } from 'antd';
import { getUser,getCabin } from '../../../utils/common';
import AppointmentsTable from '../../../layout/components/appointmentsTable/AppointmentsTable';
import 'react-toastify/dist/ReactToastify.min.css';
import { IAppointment } from '../../../interfaces/patients';
import { ToastContainer, toast } from 'react-toastify';
import { IPageData } from '../../../interfaces/page';
import { useFetchPageData, usePageData } from '../../../hooks/usePage';

const pageData: IPageData = {
  fulFilled: true,
  breadcrumbs: [
    {
      title: 'Dashboards',
      route: 'dashboard'
    },
    {
      title: 'Default'
    }
  ]
};

const DashboardPage = () => {
  let cabin=getCabin();

  const [appointments] = useFetchPageData<IAppointment[]>(`https://127.0.0.1:8000/api/rendezvouses?deleted=false&cabinet=${cabin}&datedebutrdv=${new Date().toISOString().slice(0,-14)}`, []);
  usePageData(pageData);
  const user = getUser();
  
  
toast.info(`Bienvenue ${user}!`, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: false,
                bodyClassName: "toastify-color-welcome"
            });
  return (
    <>

     
      

          <ToastContainer />

        
    
   
      <div className='row'>
        <div className='col-12 col-md-9 col-xl-4'>
          <Card style={{ background: 'rgba(251, 251, 251)' }} className='animated with-shadow'>
            <div className='row'>
              <div className='col-5'>
                <span
                  className='icofont icofont-first-aid-alt'
                  style={{ fontSize: 48, color: 'rgba(51, 108, 251, 0.5)' }}
                />
              </div>

              <div className='col-7'>
                <h6 className='mt-0 mb-1'>rendez-vous</h6>
                <div className='count' style={{ fontSize: 20, color: '#336cfb', lineHeight: 1.4 }}>
                {result}
                </div>
              </div>
            </div>
          </Card>
        </div>

        <div className='col-12 col-md-9 col-xl-4'>
          <Card style={{ background: 'rgba(251, 251, 251)' }} className='animated with-shadow'>
            <div className='row'>
              <div className='col-5'>
                <span
                  className='icofont icofont-wheelchair'
                  style={{ fontSize: 48, color: 'rgba(51, 108, 251, 0.5)' }}
                />
              </div>

              <div className='col-7'>
                <h6 className='mt-0 mb-1'>patients</h6>
                <div className='count' style={{ fontSize: 20, color: '#336cfb', lineHeight: 1.4 }}>
                  {patient}
                </div>
              </div>
            </div>
          </Card>
        </div>

        <div className='col-12 col-md-9 col-xl-4'>
          <Card style={{ background: 'rgba(251, 251, 251)' }} className='animated with-shadow'>
            <div className='row'>
              <div className='col-5'>
                <span
                  className='icofont icofont-blood'
                  style={{ fontSize: 48, color: 'rgba(51, 108, 251, 0.5)' }}
                />
              </div>

              <div className='col-7'>
                <h6 className='mt-0 mb-1'>Opérations</h6>
                <div className='count' style={{ fontSize: 20, color: '#336cfb', lineHeight: 1.4 }}>
                {operations}
                </div>
              </div>
            </div>
          </Card>
        </div>

      
      </div>

     
      <Card title="Rendez-vous d'aujourd'hui" className='mb-0'>
        <AppointmentsTable data={appointments} />
      </Card>

     

    </>
   
  );
};

export default DashboardPage;
