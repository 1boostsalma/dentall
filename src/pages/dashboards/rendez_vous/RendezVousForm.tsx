import React, { useEffect } from 'react';

import { Button, Select, Input } from 'antd';

import { useFetch } from '../../../hooks/useFetch';
import { getToken } from '../../../utils/common';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import { hasErrorFactory } from '../../../utils/hasError';
import {getCabin} from '../../../utils/common';
import { IRendezVous } from '../../../interfaces/rendez_vous';


type Props = {
  RendezVous?: IRendezVous;
  onSubmit: (RendezVous: IRendezVous) => void;
  onCancel: () => void;
  submitText?: string;
};
const { TextArea } = Input;

const defaultSubmitText = 'Ajouter';
const emptyRendezVous = {
    datedebutrdv:'',
    datefinrdv: '',
    patient: '',
    cabinet: '',
    status: '',
    description:''
};

const token = getToken();
let cabin=getCabin();
const RendezVousSchema = Yup.object().shape({
    datedebutrdv: Yup.string().required(),
    datefinrdv: Yup.string().required(),
    patient: Yup.string().required(),
   
    status: Yup.string().required(),
    description: Yup.string().required()
});

function refreshPage(){ 
  window.location.reload(); 
}

const RendezVousForm = ({
  submitText = defaultSubmitText,
  RendezVous = emptyRendezVous,
  onSubmit,
  onCancel
}: Props) => {
 
  const [patients] = useFetch<{ value: any }[]>(`https://127.0.0.1:8000/api/patients?deleted=false&cabinet_id=${cabin}`, []);

  const {
    handleSubmit ,
    handleChange,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    values,
    setValues,
    isValid,
    errors,
    touched,
    resetForm
  } = useFormik<IRendezVous>({
    validationSchema: RendezVousSchema,
    initialValues: RendezVous,
    onSubmit: (form) =>{
      if(submitText=='Ajouter')
    {
        axios.post('https://127.0.0.1:8000/api/rendezvouses',{
       
          "patient": values.patient,
          "cabinet":`${cabin}`,
          "datefinrdv":values.datefinrdv,
          "datedebutrdv":values.datedebutrdv,
          "status":values.status,
         "description":values.description,
          "deleted":false
      },
        {
          headers: { 
          'content-type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      }).then(status=> {
        
         
  
        if (status.status === 200 ||status.status === 201)
         // setStatus("Ajout success");
          refreshPage();
   
      })  
      onSubmit({ ...form });
      resetForm();
  
   }
 
   else  if(submitText=='Modifier')
    {
    axios.put(`https://127.0.0.1:8000/api/rendezvouses/${RendezVous.id}`,{
       "patient": values.patient['@id'],
          "cabinet":`${cabin}`,
          "datefinrdv":values.datefinrdv,
          "datedebutrdv":values.datedebutrdv,
          "status":values.status,
         "description":values.description,
          "deleted":false
     },{
      headers: { 
      'content-type': 'application/json',
      'Authorization': `Bearer ${token}`
    }})
   onSubmit({ ...form });
      resetForm();
  }
}});
 
     
 
 
  useEffect(() => {
    setValues({ ...values });
  }, [RendezVous]);
  
  const handlePatientSelect = (patient) => {
    setValues({ ...values, patient });
  };

  const handlestatusSelect = (value) => setFieldValue('status', value);

  const handleCancel = () => {
    resetForm();
    onCancel();
  };
 
  const hasError = hasErrorFactory(touched, errors);
 

  return (
    <>
      <form onSubmit={handleSubmit}>
      <div className='row'>
          <div className='col-sm-6 col-12'>
        Date début :
        <div className='form-group'>
          <Input
            type="datetime-local"
            name='datedebutrdv'
           
            onBlur={handleBlur}
            onChange={handleChange}
            defaultValue={values.datedebutrdv}
            className={hasError('datedebutrdv')}
          />
        </div>
       
      </div>
      <div className='col-sm-6 col-12'>
        Date fin : 
        <div className='form-group'>
          <Input
            type="datetime-local"
            defaultValue={values.datefinrdv}
            
            onBlur={handleBlur}
            name='datefinrdv'
            onChange={handleChange}
            className={hasError('datefinrdv')}
          />
        </div>
        </div>
        </div>
        <div className='row'>
        <div className='col-sm-6 col-12'>
       
        
       Status:
          <Select
                placeholder='Status'
                defaultValue={values.status}
                onChange={handlestatusSelect}
                className={hasError('status')}
                onBlur={() => setFieldTouched('status')}
              >
                <Select.Option value='En attente'>En attente</Select.Option>
                <Select.Option value='Annulé'>Annulé</Select.Option>
                <Select.Option value='Fait'>Fait</Select.Option>
                <Select.Option value='Approuvé'>Approuvé</Select.Option>
              </Select>
            </div>
            
          
          <div className='col-sm-6 col-12'>
        Patient :
        <div className='form-group'>
          <Select
            placeholder='Patient'
            defaultValue={values.patient['@id']}
            onChange={handlePatientSelect}
            className={hasError('patient')}
            onBlur={() => setFieldTouched('patient')}
          >
            {               
              patients.map((patient)=>(
            <Select.Option value={patient['@id']}>{patient['nom']} {patient['prenom']}</Select.Option>
              ))
            }
          </Select>
</div>
</div>
</div>

  <div className='row'>
       
          <TextArea
            rows={3}
            name='description'
            placeholder='Description'
            onBlur={handleBlur}
            onChange={handleChange}
            defaultValue={values.description}
            className={hasError('description')}
          />
        </div>
        <br/>
        <div className='d-flex justify-content-between buttons-list settings-actions'>
          <Button danger onClick={handleCancel}>
            Annuler
          </Button>
          <Button disabled={!isValid} type='primary' htmlType='submit'>
            {submitText}
          </Button>
        </div>
      </form>
    </>
  );
};

export default RendezVousForm;
