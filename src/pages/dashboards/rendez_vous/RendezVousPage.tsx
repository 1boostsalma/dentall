import React, { useState } from 'react';
import { Button } from 'antd';
import EditRendezVous from './EditRendezVous';

import AddRendezVous from './AddRendezVous';
import RendezVousTable from '../../../layout/components/rendez_vous/RendezVousTable';
import PageAction from '../../../layout/components/page-action/PageAction';
import { useFetchPageData, usePageData } from '../../../hooks/usePage';
import { IRendezVous } from '../../../interfaces/rendez_vous';
import { IPageData } from '../../../interfaces/page';
import axios from 'axios';
import { getToken } from '../../../utils/common';
import { getCabin } from '../../../utils/common';
const pageData: IPageData = {
  title: 'Rendez Vous',
  fulFilled: false,
  breadcrumbs: [
    {
      title: 'Dashboard',
      route: 'default-dashboard'
    },
    {
      title: 'RendezVous'
    }
  ]
};
const RendezVousPage = () => {

  usePageData(pageData);

let cabin=getCabin();
  const [RendezVouss, setRendezVouss] = useFetchPageData<IRendezVous[]>(
    `https://127.0.0.1:8000/api/rendezvouses?deleted=false&cabinet=${cabin}`,
    []
    ); 
    let token = getToken();  
   
  const [selectedRendezVous, setSelectedRendezVous] = useState(null);
  const [addingModalVisibility, setAddingModalVisibility] = useState(false);
  const handleDelete = (RendezVous: IRendezVous) => {
 
    axios.put(`https://127.0.0.1:8000/api/rendezvouses/${RendezVous.id}`,{
      "deleted": true
    },{
    headers: { 
    'content-type': 'application/json',
    'Authorization': `Bearer ${token}`
  }});
    const newRendezVouss = RendezVouss.filter((el) => el !== RendezVous);
    setRendezVouss(newRendezVouss);
  };
  
  const handleEdit = (RendezVous: IRendezVous) => {
   
    const editedRendezVouss = RendezVouss.map((el) =>
      el !== selectedRendezVous ? el : RendezVous
    );
    setRendezVouss(editedRendezVouss);
    setSelectedRendezVous(null);
  };

  const openAddingModal = () => setAddingModalVisibility(true);
  const addRendezVous = (RendezVous: IRendezVous) => {
    setRendezVouss([RendezVous, ...RendezVouss]);
    setAddingModalVisibility(false);
  };
  const closeAddingModal = () => setAddingModalVisibility(false);
  const openEditModal = (RendezVous: IRendezVous) => setSelectedRendezVous(RendezVous);
  const closeModal = () => {
    setSelectedRendezVous(null);
  };
  const RendezVousActions = (RendezVous: IRendezVous) => (
    <div className='buttons-list nowrap'>
      <Button onClick={openEditModal.bind({}, RendezVous)} shape='circle' type='primary'>
        <span className='icofont icofont-edit-alt' />
      </Button>
      <Button onClick={handleDelete.bind({}, RendezVous)} shape='circle' danger>
        <span className='icofont icofont-ui-delete' />
      </Button>
    </div>
  );
  return (
    <>
      <PageAction onClick={openAddingModal} icon='icofont-plus' type={'primary'} />
      <RendezVousTable data={RendezVouss} actions={RendezVousActions} />
      <AddRendezVous
        onClose={closeAddingModal}
        visible={addingModalVisibility} 
        onSubmit={addRendezVous}
      />
      <EditRendezVous
        RendezVous={selectedRendezVous}
        visible={!!selectedRendezVous}
        onClose={closeModal}
        onEdited={handleEdit}
      />
     
    </>
  );
};
export default RendezVousPage;
