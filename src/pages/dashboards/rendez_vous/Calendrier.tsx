import React, { useState } from 'react';
import { Button, Card, Modal } from 'antd';
import {IPageData} from '../../../interfaces/page';
import { IRendezVous } from '../../../interfaces/rendez_vous';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import { useFetchPageData, usePageData } from '../../../hooks/usePage';
import { getToken } from '../../../utils/common';
import {getCabin}from '../../../utils/common';

const headerOptions = {
  left: 'prev,next today',
  center: 'title',
  right: 'dayGridMonth,dayGridWeek,dayGridDay'
};
const pageData: IPageData = {
  title: 'RDV calendar',
  fulFilled: true,
  breadcrumbs: [
    {
      title: 'Apps',
      route: 'default-dashboard'
    },
    {
      title: 'Service pages',
      route: 'default-dashboard'
    },
    {
      title: 'Events calendar'
    }
  ]
};

const Calendrier = () => {
  usePageData(pageData);
  const [event, setEvent] = useState(null);
  let token = getToken();
  const [modalVisibility, setModalVisibility] = useState(false);
  const [selectedRendezVous, setSelectedRendezVous] = useState(null);
  const setDate = (day: number, hour: number = 0) => {
    const date = new Date();

    date.setDate(date.getDate() + day);
    date.setHours(date.getHours() + hour);

    return date;
  };
  let cabin=getCabin();
  const [RendezVouss, setRendezVouss] = useFetchPageData<IRendezVous[]>(
    `https://127.0.0.1:8000/api/rendezvouses?deleted=false&cabinet=${cabin}`,
  [],);
  let rdv_color='';
  let color=''
  let rdvs = [];

 
  for(var i=0; i<RendezVouss.length; i++){
    
    switch(RendezVouss[i].status){
      case 'Fait':
         rdv_color='event-green';
         color='#b7ce63';
         break;
      case 'Approuvé':
         rdv_color='event-yellow';
         color='#cec759';
         break;
      case 'En attente':
         rdv_color='event-pink';
         color='#336CFB';
         break;
      case 'Annulé':
         rdv_color='event-red'; 
         color='#ed5564' ; 
         break;
    }
    rdvs.push(
      {
        title:'Rendez vous',
        patient: RendezVouss[i].patient['nom'],
        patientpre: RendezVouss[i].patient['prenom'],
      
        description:RendezVouss[i].description,
        status: RendezVouss[i].status,
        classNames: [rdv_color],
        code:RendezVouss[i].id,
        start: RendezVouss[i].datedebutrdv,
        end: RendezVouss[i].datefinrdv,
        color:color
       
      } 
  );
}
  

  
  
const closeModal = () => setModalVisibility(false);

const handleEventClick = (arg: any) => {
  setEvent(arg.event);
  setModalVisibility(true);
};

    
  let modalBody, modalTitle, modalFooter;
  if (event) {
     
    modalBody = (
      <div className='d-flex flex-column'>
        <div className='event-time flex-column mb-4'>
          <h5 className='event-title m-0'>Heure de l'évènement </h5>
          <span>
            de: {event._instance.range.start.toDateString()}- à: {event._instance.range.end.toDateString()}
          </span>
        </div>

        <div className='event-desc flex-column'>
          <h5 className='event-title m-0'>Description de l'évenement</h5>
          <span>
          <b>Status: </b> {event.extendedProps.status}
          </span><br/>
          <span>
          <b>Patient: </b>  {event.extendedProps.patient} {event.extendedProps.patientpre}
          </span><br/>
          <span>
          <b>Cabinet: </b>  {event.extendedProps.cabinet}
          </span><br/>
           <span>
          <b>Description: </b>  {event.extendedProps.description}
          </span>
        </div>
      </div>
    );

    modalTitle = (
      <div className='title-block p-0 m-0'>
        <h3 style={{ color: event.backgroundColor }} className='modal-title m-0'>
          {event.title}
        </h3>
      </div>
    );
  
    modalFooter = (
     
      
        <div className='d-flex justify-content-between modal-footer'>
         
        <Button onClick={closeModal} danger>
          Annuler
        </Button>
       
        
      </div> 
       
    );
   
  }  


  
  return (
    <>
      <Card className='mb-0'>
        <FullCalendar
          eventClick={handleEventClick}
          events={rdvs}
          headerToolbar={headerOptions}
          initialView='dayGridMonth'
          plugins={[dayGridPlugin]}
          dayMaxEvents={true}
          weekends
        />
      </Card>

      <Modal
        title={modalTitle}
        footer={modalFooter}
        visible={modalVisibility}
        onCancel={closeModal}
      >
        {modalBody}
      </Modal>
      
       
    </>
     
  );
};

export default Calendrier;
