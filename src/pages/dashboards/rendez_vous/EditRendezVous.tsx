import React from 'react';
import { IRendezVous } from '../../../interfaces/rendez_vous';
import RendezVousForm from './RendezVousForm';
import { Modal } from 'antd';


type Props = {
  onEdited: (IRendezVous) => void;
  RendezVous: IRendezVous;
  visible: boolean;
  onClose: () => void;
};

const EditRendezVous = ({ RendezVous, visible, onClose, onEdited }: Props) => {
  return (
     
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Modifier RendezVous</h3>}
    >
      <RendezVousForm
        onCancel={onClose}
        onSubmit={onEdited}
        RendezVous={RendezVous}
        submitText='Modifier'
      />
    </Modal>
   
  );
 
};

export default EditRendezVous;
