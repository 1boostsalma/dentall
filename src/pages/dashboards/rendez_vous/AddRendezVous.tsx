import React from 'react';
import { getToken } from '../../../utils/common';
import { Modal } from 'antd';

import RendezVousForm from './RendezVousForm';
import { IRendezVous } from '../../../interfaces/rendez_vous';
type Props = {
  onSubmit: (RendezVous: IRendezVous) => void;
  visible: boolean;
  onClose: () => void;
};

const AddRendezVous = ({ visible, onClose, onSubmit }: Props) => {
 
 
  return (
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Ajouter Rendez vous</h3>}
    >
      <RendezVousForm onCancel={onClose} onSubmit={onSubmit} submitText='Ajouter' />
    </Modal>
  );
};

export default AddRendezVous;
