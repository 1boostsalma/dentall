import React from 'react';
import { IPatient } from '../../../interfaces/patients';
import PatientForm from './PatientForm';
import { Modal } from 'antd';

type Props = {
  onEdited: (IPatient) => void;
  Patient: IPatient;
  visible: boolean;
  onClose: () => void;
};

const EditPatient = ({ Patient, visible, onClose, onEdited }: Props) => {
  return (
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Modifier Patient</h3>}
    >
      <PatientForm
        onCancel={onClose}
        onSubmit={onEdited}
        patient={Patient}
        submitText='Modifier'
      />
    </Modal>
  );
};

export default EditPatient;
