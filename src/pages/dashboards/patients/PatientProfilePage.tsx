import React, { useState ,useEffect} from 'react';
import { Card, Form, Input, Select, Timeline,Table } from 'antd';
import { useLocation } from "react-router-dom";
import { useFormik } from 'formik';
import { useFetch } from '../../../hooks/useFetch';
import PaymentsTable from '../../../layout/components/Payments/PaymentsTable';

import { IPageData } from '../../../interfaces/page';
import {getCabin ,getToken} from '../../../utils/common';

import { usePageData } from '../../../hooks/usePage';
import { useGetPatient } from '../../../hooks/useGetPatient';
import { useGetBillings } from '../../../hooks/useGetBillings';
import BillingTable from '../../medic/components/BillingTable';


const pageData: IPageData = {
  title: 'Patient profile page',
  fulFilled: true,
  breadcrumbs: [
    {
      title: 'Medicine',
      route: 'default-dashboard'
    },
    {
      title: 'Patients',
      route: 'default-dashboard'
    },
    
  ]
};

let token = getToken();
const cabin=getCabin();
const FormItem = Form.Item;
const Option = Select.Option;

const ProfileForm = ({ patient }) => {
  const { values } = useFormik({
    initialValues: { ...patient },
    onSubmit: () => null
  });

  const [assurances] = useFetch<{ value: any }[]>('https://127.0.0.1:8000/api/assurances?deleted=false', []);
  return (
    <Form layout='vertical'>
       <div className='form-group'>
       
        </div>
      <div className='row'>
        <div className='col-md-6 col-sm-12'>
      <FormItem label='nom'>
        <Input defaultValue={values.nom} placeholder='Nom' />
      </FormItem>
       </div>
       <div className='col-md-6 col-sm-12'>
      <FormItem label='prenom'>
        <Input defaultValue={values.prenom} placeholder='Prénom' />
      </FormItem>
</div>
</div>
      <div className='row'>
        <div className='col-md-6 col-sm-12'>
          <FormItem label='assurance'>
          <Select
           placeholder='assurances'
           defaultValue={values.assurance}
         >
           {               
           assurances.map((assurance)=>(
           <Select.Option value={assurance['@id']}>{assurance['libelleAssurance']}</Select.Option>
           ))  
           }
           
         </Select>
          </FormItem>
        </div>
        <div className='col-md-6 col-sm-12'>
          <FormItem label='sexe'>
            <Select defaultValue={values.sexe} placeholder='Sexe'>
            <Option value='Homme'>Homme</Option>
                <Option value='Femme'>Femme</Option>
            </Select>
          </FormItem>
        </div>
      </div>
      <div className='row'>
        <div className='col-md-6 col-sm-12'>
        <FormItem label='cin'>
          <Input defaultValue={values.cin} placeholder='CIN' />
          </FormItem>
        </div>
        <div className='col-md-6 col-sm-12'>
      <FormItem label='Telephone'>
        <Input defaultValue={values.telephone} placeholder='Telephone' />
      </FormItem>
</div>
</div>
      <FormItem label='Adresse'>
        <Input.TextArea rows={4} defaultValue={values.adresse} placeholder='Adresse' />
      </FormItem>
    </Form>
  );
};



export var patients; 
const PatientProfilePage = () => {
  
 
  var location = useLocation();
patients=(location.state).toString();
 const {patient}= useGetPatient((location.state).toString());
  
  const billings = useGetBillings();
  usePageData(pageData);
  

  return (
    patient && (
      <>
      
        <div className='row mb-4'>
          <div className='col-md-6 col-sm-12'>
           

            <div className='info stack'>
              <ProfileForm patient={patient} />

            </div>
          </div>

          <div className='col-md-6 col-sm-12'>
           
          </div>
        </div>
        <Card title='Billings' className='mb-0'>
          <BillingTable billings={billings} />
        </Card>
      
        
      
      </>
    )
  );
};

export default PatientProfilePage;
