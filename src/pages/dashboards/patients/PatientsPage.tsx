import React, { useState } from 'react';
import { Button } from 'antd';
import EditPatient from './EditPatient';
import AddPatient from './AddPatient';
import PatientsTable from '../../../layout/components/patients/PatientsTable';
import PageAction from '../../../layout/components/page-action/PageAction';
import { useFetchPageData, usePageData } from '../../../hooks/usePage';
import { IPatient } from '../../../interfaces/patients';
import { IPageData } from '../../../interfaces/page';
import {getCabin} from '../../../utils/common';
import axios from 'axios';
import { history } from '../../../redux/store';
import { getToken } from '../../../utils/common';

const pageData: IPageData = {
  title: 'Patients',
  fulFilled: true,
  breadcrumbs: [
    {
      title: 'dashboard',
      route: 'default-dashboard'
    },
    {
      title: 'Patients'
    }
  ]
};
const cabin=getCabin();

const PatientsPage = () => {
  usePageData(pageData);
  let token = getToken();
  const [Recherche,setRecherche] = useState(null);

  const [Patients, setPatients] = useFetchPageData<IPatient[]>(`https://127.0.0.1:8000/api/patients?deleted=false&cabinet=${cabin}`,
    []
    ); 

    
  const [selectedPatient, setSelectedPatient] = useState(null);
  const [addingModalVisibility, setAddingModalVisibility] = useState(false);
  
  const handleDelete = (Patient: IPatient) => {
    
    axios.put(`https://127.0.0.1:8000/api/patients/${Patient.id}`,{
      "deleted": true
    },{
    headers: { 
    'content-type': 'application/json',
    'Authorization': `Bearer ${token}`
  }});
    const newPatients = Patients.filter((el) => el !== 

Patient);
    setPatients(newPatients);
  };
  
  const handleEdit = (Patient: IPatient) => {
    const editedPatients = Patients.map((el) =>
      el !== selectedPatient ? el : Patient
    );
    setPatients(editedPatients);
    setSelectedPatient(null);
  };
  const openAddingModal = () => 

setAddingModalVisibility(true);
  const addPatient = (Patient: IPatient) => {
    setPatients([Patient, ...Patients]);
    setAddingModalVisibility(false);
  };
  const closeAddingModal = () => 

setAddingModalVisibility(false);
  const openEditModal = (Patient: IPatient) => 

setSelectedPatient(Patient);
  const closeModal = () => {
    setSelectedPatient(null);
  };


  const handleShowInfo = (Patient: IPatient) => history.push({pathname: '/vertical/patient-profile',state: Patient['@id']}); 

 // const handleShowInfo = (Patient: IPatient) => history.push({pathname: '../../../hooks/useGetBillings',state: Patient['@id']}); 
  const PatientsActions = (Patient: IPatient) => (

    <div className='buttons-list nowrap'>
       
      <Button shape='circle' onClick={handleShowInfo.bind({},Patient)}>
     
              <span className='icofont icofont-external-link' />
      </Button>
      
      <Button onClick={openEditModal.bind({},Patient)} shape='circle' type='primary'>
        <span className='icofont icofont-edit-alt' />
      </Button>
      <Button onClick={handleDelete.bind({}, Patient)} shape='circle' danger>
        <span className='icofont icofont-ui-delete' 

/>
      </Button>

    </div>
    
  );

  return (
    <>
  <PageAction onClick={openAddingModal} icon='icofont-plus' type={'primary'} />
 
  
 <PatientsTable data={Patients} actions={PatientsActions} />


     
      <AddPatient
        onClose={closeAddingModal}
        visible={addingModalVisibility} 
        onSubmit={addPatient}
      />
      <EditPatient
        Patient={selectedPatient}
        visible={!!selectedPatient}
        onClose={closeModal}
        onEdited={handleEdit}
      />
      
    </>
  );
};
export default PatientsPage;