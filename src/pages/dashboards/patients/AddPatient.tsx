import React from 'react';
import { getToken } from '../../../utils/common';
import { Modal } from 'antd';

import PatientForm from './PatientForm';
import { IPatient } from '../../../interfaces/patients';
type Props = {
  onSubmit: (Patient: IPatient) => void;
  visible: boolean;
  onClose: () => void;
};

const AddPatient = ({ visible, onClose, onSubmit }: Props) => {
 const token = getToken();
 
  return (
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Ajouter</h3>}
    >
      <PatientForm onCancel={onClose} onSubmit={onSubmit} submitText='Ajouter' />
    </Modal>
  );
};

export default AddPatient;
