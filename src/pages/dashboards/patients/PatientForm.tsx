import React, { useEffect, useState } from 'react';
import { Button, Select, Input, AutoComplete } from 'antd';
import { useFetch } from '../../../hooks/useFetch';
import {getCabin} from '../../../utils/common';
import { getToken } from '../../../utils/common';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import { hasErrorFactory } from '../../../utils/hasError';
import { IPatient } from '../../../interfaces/patients';
//import { AnyARecord } from 'dns';
type Props = {
  patient?: IPatient;
  onSubmit: (Patient: IPatient) => void;
  onCancel: () => void;
  submitText?: string;
};
const { TextArea } = Input;
const defaultSubmitText = 'Ajouter';
const emptyPatient = {
  id:'',
  cabinet:'',	
  nom:'',	
  cin:'',
  prenom:'',	
  telephone:'',	
  datenaissance:'',
  sexe:'',
  adresse:'',
  assurance:'',
  deleted:'false'
};

const token = getToken();
const cabin=getCabin();
const PatientSchema = Yup.object().shape({
  nom: Yup.string().required(),
  sexe: Yup.string().required(),
  cin: Yup.string().required(),
  datenaissance: Yup.string().required(),
  prenom: Yup.string().required(),
  adresse: Yup.string().required(),
  assurance: Yup.string().required(),
  telephone: Yup.string().required()
});
function refreshPage(){ 
  window.location.reload(); 
}
const PatientForm = ({
  submitText = defaultSubmitText,
  patient = emptyPatient,
  onSubmit,
  onCancel
}: Props) => {

  const [assurances] = useFetch<{ value: any }[]>('https://127.0.0.1:8000/api/assurances?deleted=false', []);
 
  const {
    handleSubmit ,
    handleChange,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    values,
    setValues,
    isValid,
    errors,
    touched,
    resetForm
  } = useFormik<IPatient>({
    validationSchema: PatientSchema,
    initialValues: patient,
    
    onSubmit: (form) =>{
      if(submitText=='Ajouter')
    {
      axios.post('https://127.0.0.1:8000/api/patients',{
     //"id":values.id,
     "nom": values.nom,
     "cin": values.cin,
     "prenom": values.prenom,
     "cabinet":`${cabin}`,
     "datenaissance":values.datenaissance,
     "adresse":values.adresse,
     "sexe":values.sexe,
     "assurances":values.assurance,
     "telephone": values.telephone,
     "deleted":false
    },
      {
        headers: { 
        'content-type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
      }).then(status=> {
        if (status.status === 200 ||status.status === 201)
           // setStatus("Ajout success");
            refreshPage();
     
        }).catch(error => {        
         if (error.response.status)
          
         refreshPage();
        });         
      onSubmit({ ...form });
      resetForm();
     
   }
   else  if(submitText=='Modifier')
    {
    axios.put(`https://127.0.0.1:8000/api/patients/${patient.id}`,{
      "cin": values.cin,
       "nom": values.nom,
        "prenom": values.prenom,
        "cabinet":`${cabin}`,
        "datenaissance":values.datenaissance,
        "adresse":values.adresse,
        "sexe":values.sexe,
        "assurances":values.assurance,
        "telephone": values.telephone,
        "deleted":false
     },{
      headers: { 
      'content-type': 'application/json',
      'Authorization': `Bearer ${token}`
    }}).then(status=> {
      if (status.status === 200 ||status.status === 201)
         // setStatus("Ajout success");
          refreshPage();
   
      }).catch(error => {        
       if (error.response.status)
        
       refreshPage();
      });     
   onSubmit({ ...form });
      resetForm();
  }
}});
     
  useEffect(() => {
    setValues({ ...values });
  }, [patient]);

 const handleTypesSoinsSelect = (assurance) => {
    setValues({ ...values, assurance });
 };


  const handleCancel = () => {
    resetForm();
    onCancel();
  };
   const handleGenderSelect = (value) => setFieldValue('sexe', value);
  const hasError = hasErrorFactory(touched, errors);

  return (
    <>
      <form onSubmit={handleSubmit}>
      <div className='form-group'>
          <Input
            name='cin'
            placeholder='CIN'
            onBlur={handleBlur}
            onChange={handleChange}
            defaultValue={values.cin}
            className={hasError('cin')}
          />
        </div>
        <div className='row'>
          <div className='col-sm-6 col-12'>
        <div className='form-group'>
          <Input
            name='nom'
            placeholder='Nom'
            onBlur={handleBlur}
            onChange={handleChange}
            defaultValue={values.nom}
            className={hasError('nom')}
          />
        </div>
        </div>
        
        <div className='col-sm-6 col-12'>
        <div className='form-group'>
          <Input
            defaultValue={values.prenom}
            placeholder='Prenom'
            onBlur={handleBlur}
            name='prenom'
            onChange={handleChange}
            className={hasError('prenom')}
          />
        </div> 
</div>
</div>
      <div className='row'>
          <div className='col-sm-6 col-12'>
            <div className='form-group'>
          
          <Input
            type="date"
            defaultValue={values.datenaissance}
            placeholder='Date'
            name='datenaissance'
            onChange={handleChange}
            onBlur={handleBlur}
            className={hasError('datenaissance')}
          />
        </div>
          </div>
        <div className='col-sm-6 col-12'>
            <div className='form-group'>
              <Select
                placeholder='Sexe'
                defaultValue={values.sexe}
                onChange={handleGenderSelect}
                className={hasError('sexe')}
                onBlur={() => setFieldTouched('sexe')}
              >
                <Select.Option value='Homme'>Homme</Select.Option>
                <Select.Option value='Femme'>Femme</Select.Option>
              </Select>
            </div>
          </div>
        </div>
        <div className='row'>
          <div className='col-sm-6 col-12'>
            <div className='form-group'>
       
          <Input
            type='telephone'
            name='telephone'
            onBlur={handleBlur}
            placeholder='Telephone'
            onChange={handleChange}
            defaultValue={values.telephone}
            className={hasError('telephone')}
          />
        </div>
        </div>
        <div className='col-sm-6 col-12'>
            <div className='form-group'>
            <Select
           placeholder='assurances'
           defaultValue={values.assurance}
           onChange={handleTypesSoinsSelect}
           className={hasError('assurance')}
           onBlur={() => setFieldTouched('assurance')}
         >
           {               
           assurances.map((assurance)=>(
           <Select.Option value={assurance['@id']}>{assurance['libelleAssurance']}</Select.Option>
           ))  
           }
           
         </Select>
</div>
</div>
</div>
        <div className='form-group'>
          <TextArea
            rows={3}
            name='adresse'
            placeholder='Adresse'
            onBlur={handleBlur}
            onChange={handleChange}
            defaultValue={values.adresse}
            className={hasError('adresse')}
          />
        </div>
        <div className='d-flex justify-content-between buttons-list settings-actions'>
          <Button danger onClick={handleCancel}>
            Annuler
          </Button>
          <Button disabled={!isValid} type='primary' htmlType='submit'>
            {submitText}
          </Button>
        </div>
      </form>
    </>
  );
};
export default PatientForm;