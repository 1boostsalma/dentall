import React, { useState } from 'react';
import { Button } from 'antd';

import OperationsTable from '../../../layout/components/operations/OperationsTable';
import PageAction from '../../../layout/components/page-action/PageAction';
import { useFetchPageData, usePageData } from '../../../hooks/usePage';
import { IOperation } from '../../../interfaces/operations';
import { IPageData } from '../../../interfaces/page';
import {getCabin} from '../../../utils/common';
import axios from 'axios';
import { history } from '../../../redux/store';
import { getToken } from '../../../utils/common';

const pageData: IPageData = {
  title: 'Operations',
  fulFilled: true,
  breadcrumbs: [
    {
      title: 'dashboard',
      route: 'default-dashboard'
    },
    {
      title: 'Operations'
    }
  ]
};
const cabin=getCabin();

const OperationsPage = () => {
  usePageData(pageData);
  const [Operations, setOperations] = useFetchPageData<IOperation[]>(
    `https://127.0.0.1:8000/api/operations?deleted=false&cabinet=${cabin}`,
    []
    ); 
  
  const [selectedPatient, setSelectedOperations] = 

useState(null);
  const [addingModalVisibility, 

setAddingModalVisibility] = useState(false);
  const handleDelete = (Operation: IOperation) => {
    let token = getToken();
    axios.put(`https://127.0.0.1:8000/api/operations/${Operation.id}`,{
      "deleted": true
    },{
    headers: { 
    'content-type': 'application/json',
    'Authorization': `Bearer ${token}`
  }});
    const newOperations = Operations.filter((el) => el !== 

Operation);
    setOperations(newOperations);
  };
  
 
const openAddingModal = () => {

  // window.location.hash = "/vertical/Ajout";
  history.push('Ajout');

  };

 
 


  const handleShowInfo = (Operation: IOperation) => history.push({pathname: 'edit',state: Operation['@id']}); 
    

  const OperationsActions = (Operation: IOperation) => (

    <div className='buttons-list nowrap'>
       
      <Button shape='circle' onClick={handleShowInfo.bind({},Operation)}  type='primary'>
     
               <span className='icofont icofont-edit-alt' />
      </Button>
      
      
      <Button onClick={handleDelete.bind({}, Operation)} shape='circle' danger>
        <span className='icofont icofont-ui-delete' 

/>
      </Button>

    </div>
  );
  
  return (
    <>
      <PageAction onClick={openAddingModal} icon='icofont-plus' type={'primary'} />
      <OperationsTable data={Operations} actions={OperationsActions} />
     
      
    </>
  );
};
export default OperationsPage;