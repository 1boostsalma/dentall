import React from 'react';
import { getToken } from '../../../utils/common';
import { Modal } from 'antd';

import SoinForm from './SoinForm';
import { ITypesSoins } from '../../../interfaces/TypesSoins';
type Props = {
  onSubmit: (TypesSoins: ITypesSoins) => void;
  visible: boolean;
  onClose: () => void;
};

const AddTypesSoins = ({ visible, onClose, onSubmit }: Props) => {
 const token = getToken();
 
  return (
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Ajouter</h3>}
    >
      <SoinForm onCancel={onClose} onSubmit={onSubmit} submitText='Ajouter' />
    </Modal>
  );
};

export default AddTypesSoins;
