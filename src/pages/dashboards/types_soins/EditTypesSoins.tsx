import React from 'react';
import { ITypesSoins } from '../../../interfaces/TypesSoins';
import SoinForm from './SoinForm';
import { Modal } from 'antd';

type Props = {
  onEdited: (ITypesSoins) => void;
  TypesSoins: ITypesSoins;
  visible: boolean;
  onClose: () => void;
};

const EditTypesSoins = ({ TypesSoins, visible, onClose, onEdited }: Props) => {
  return (
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Modifier</h3>}
    >
      <SoinForm
        onCancel={onClose}
        onSubmit={onEdited}
        TypesSoins={TypesSoins}
        submitText='Modifier'
      />
    </Modal>
  );
};

export default EditTypesSoins;
