import React, { useEffect } from 'react';
import { Button, Select, Input } from 'antd';
import {getCabin} from '../../../utils/common';
import { getToken } from '../../../utils/common';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import { hasErrorFactory } from '../../../utils/hasError';
import { ITypesSoins } from '../../../interfaces/TypesSoins';
//import { AnyARecord } from 'dns';
type Props = {
  TypesSoins?: ITypesSoins;
  onSubmit: (TypesSoins: ITypesSoins) => void;
  onCancel: () => void;
  submitText?: string;
};

const defaultSubmitText = 'Ajouter';
const emptyTypesSoins = {
 
 type:'',
 cabinet:'',
  deleted:'false'
};
const token = getToken();
const cabin=getCabin();
const TypesSoinsSchema = Yup.object().shape({
  type: Yup.string().required(),
  
});
function refreshPage(){ 
  window.location.reload(); 
}
const SoinForm = ({
  submitText = defaultSubmitText,
  TypesSoins = emptyTypesSoins,
  onSubmit,
  onCancel
}: Props) => {
  
  const {
    handleSubmit ,
    handleChange,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    values,
    setValues,
    isValid,
    errors,
    touched,
    resetForm
  } = useFormik<ITypesSoins>({
    validationSchema: TypesSoinsSchema,
    initialValues: TypesSoins,
    
    onSubmit: (form) =>{
      if(submitText=='Ajouter')
    {
      axios.post('https://127.0.0.1:8000/api/types_soins',{
        "type": values.type,
        "cabinet":cabin,
        "deleted":false
    },
      {
        headers: { 
        'content-type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
      })    
      onSubmit({ ...form });
     
      resetForm();
 
   }
   else  if(submitText=='Modifier')
    {
    axios.put(`https://127.0.0.1:8000/api/types_soins/${TypesSoins.id}`,{
      "type": values.type,
      
        "cabinet":cabin,
        "deleted":false
     },{
      headers: { 
      'content-type': 'application/json',
      'Authorization': `Bearer ${token}`
    }})
   onSubmit({ ...form });
      resetForm();
  }
}});
   
  useEffect(() => {
    setValues({ ...values });
  }, [TypesSoins]);

 
 //const handleStatusSelect = (value) => setFieldValue('status', value);

  const handleCancel = () => {
    resetForm();
    onCancel();
  };
  
  const hasError = hasErrorFactory(touched, errors);

  return (
    <>
      <form onSubmit={handleSubmit}>
      <div className='form-group'>
          <Input
            name='type'
            placeholder='TYPE'
            onBlur={handleBlur}
            onChange={handleChange}
            defaultValue={values.type}
            className={hasError('type')}
          />
        </div>

        <div className='d-flex justify-content-between buttons-list settings-actions'>
          <Button danger onClick={handleCancel}>
            Annuler
          </Button>
          <Button disabled={!isValid} type='primary' htmlType='submit'>
            {submitText}
          </Button>
        </div>
      </form>
    </>
  );
};
export default SoinForm;