import React, { useState } from 'react';
import { Button } from 'antd';
import EditTypesSoins from './EditTypesSoins';
import AddTypesSoins from './AddTypesSoins';
import TypesSoinsTable from '../../../layout/components/TypesSoins/TypesSoinsTable';
import PageAction from '../../../layout/components/page-action/PageAction';
import { useFetchPageData, usePageData } from '../../../hooks/usePage';
import { ITypesSoins } from '../../../interfaces/TypesSoins';
import { IPageData } from '../../../interfaces/page';
import {getCabin} from '../../../utils/common';
import axios from 'axios';

import { getToken } from '../../../utils/common';

const pageData: IPageData = {
  title: 'TypesSoins',
  fulFilled: true,
  breadcrumbs: [
    {
      title: 'dashboard',
      route: 'default-dashboard'
    },
    {
      title: 'TypesSoins'
    }
  ]
};
const cabin=getCabin();

const TypesSoinsPage = () => {
  usePageData(pageData);
  const [TypesSoinss, setTypesSoinss] = useFetchPageData<ITypesSoins[]>(
    `https://127.0.0.1:8000/api/types_soins?deleted=false&cabinet=${cabin}`,
    []
    ); 
  
  const [selectedTypesSoins, setSelectedTypesSoins] = useState(null);
  const [addingModalVisibility, setAddingModalVisibility] = useState(false);
  const handleDelete = (TypesSoins: ITypesSoins) => {
    let token=getToken();
    axios.put(`https://127.0.0.1:8000/api/types_soins/${TypesSoins.id}`,{
     
        "deleted":true
     },{
      headers: { 
      'content-type': 'application/json',
      'Authorization': `Bearer ${token}`
    }})
    const newTypesSoinss = TypesSoinss.filter((el) => el !== TypesSoins);
    setTypesSoinss(newTypesSoinss);
  };
  
  const handleEdit = (TypesSoins: ITypesSoins) => {
    const editedTypesSoinss = TypesSoinss.map((el) =>
      el !== selectedTypesSoins ? el : TypesSoins
    );
    setTypesSoinss(editedTypesSoinss);
    setSelectedTypesSoins(null);
  };
  const openAddingModal = () => 

setAddingModalVisibility(true);
  const addTypesSoins = (TypesSoins: ITypesSoins) => {
    setTypesSoinss([TypesSoins, ...TypesSoinss]);
    setAddingModalVisibility(false);
  };
  const closeAddingModal = () => 

setAddingModalVisibility(false);
  const openEditModal = (TypesSoins: ITypesSoins) => 

setSelectedTypesSoins(TypesSoins);
  const closeModal = () => {
    setSelectedTypesSoins(null);
  };





  const TypesSoinsActions = (TypesSoins: ITypesSoins) => (

    <div className='buttons-list nowrap'>
       
    
      
      <Button onClick={openEditModal.bind({},TypesSoins)} shape='circle' type='primary'>
        <span className='icofont icofont-edit-alt' />
      </Button>
      <Button onClick={handleDelete.bind({}, TypesSoins)} shape='circle' danger>
        <span className='icofont icofont-ui-delete' 

/>
      </Button>

    </div>
  );
  
  return (
    <>
      <PageAction onClick={openAddingModal} 

icon='icofont-plus' type={'primary'} />
      <TypesSoinsTable data={TypesSoinss} actions=

{TypesSoinsActions} />
      <AddTypesSoins
        onClose={closeAddingModal}
        visible={addingModalVisibility} 
        onSubmit={addTypesSoins}
      />
      <EditTypesSoins
        TypesSoins={selectedTypesSoins}
        visible={!!selectedTypesSoins}
        onClose={closeModal}
        onEdited={handleEdit}
      />
      
    </>
  );
};
export default TypesSoinsPage;