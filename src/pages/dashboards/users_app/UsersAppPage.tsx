import React, { useState } from 'react';
import { Button } from 'antd';
import EditUserApp from './EditUserApp';
import AddUserApp from './AddUserApp';
import UserAppsTable from '../../../layout/components/users_app/UsersAppTable';
import PageAction from '../../../layout/components/page-action/PageAction';
import { useFetchPageData, usePageData } from '../../../hooks/usePage';
import { IUserApp } from '../../../interfaces/userApps';
import { IPageData } from '../../../interfaces/page';
import axios from 'axios';
import { getToken } from '../../../utils/common';
 
const pageData: IPageData = {
  title: 'Utilisateurs/Application',
  fulFilled: false,
  breadcrumbs: [
    {
      title: 'Dashboard',
      route: 'default-dashboard'
    },
    {
      title: 'Utilisateurs/Application'
    }
  ]
};
const UserAppsPage = () => {
  usePageData(pageData);
  const [UserApps, setUserApps] = useFetchPageData<IUserApp[]>(
    'https://127.0.0.1:8000/api/user_apps?deleted=false',
    []
    );   
  const [selectedUserApp, setSelectedUserApp] = useState(null);
  const [addingModalVisibility, setAddingModalVisibility] = useState(false);
  const handleDelete = (UserApp: IUserApp) => {
    let token = getToken();
    axios.put(`https://127.0.0.1:8000/api/user_apps/${UserApp.id}`,{
      "deleted": true
    },{
    headers: { 
    'content-type': 'application/json',
    'Authorization': `Bearer ${token}`
  }});
    const newUserApps = UserApps.filter((el) => el !== UserApp);
    setUserApps(newUserApps);
  };
  const handleEdit = (UserApp: IUserApp) => {
    const editedUserApps = UserApps.map((el) =>
      el !== selectedUserApp ? el : UserApp
    );
    setUserApps(editedUserApps);
    setSelectedUserApp(null);
  };
  const openAddingModal = () => setAddingModalVisibility(true);
  const addUserApp = (UserApp: IUserApp) => {
    setUserApps([UserApp, ...UserApps]);
    setAddingModalVisibility(false);
  };
  const closeAddingModal = () => setAddingModalVisibility(false);
  const openEditModal = (UserApp: IUserApp) => setSelectedUserApp(UserApp);
  const closeModal = () => {
    setSelectedUserApp(null);
  };
  const UserAppsActions = (UserApp: IUserApp) => (
    <div className='buttons-list nowrap'>
      <Button onClick={openEditModal.bind({}, UserApp)} shape='circle' type='primary'>
        <span className='icofont icofont-edit-alt' />
      </Button>
      <Button onClick={handleDelete.bind({}, UserApp)} shape='circle' danger>
        <span className='icofont icofont-ui-delete' />
      </Button>
    </div>
  );
  return (
    <>
      <PageAction onClick={openAddingModal} icon='icofont-plus' type={'primary'} />
      <UserAppsTable data={UserApps} actions={UserAppsActions} />
      <AddUserApp
        onClose={closeAddingModal}
        visible={addingModalVisibility} 
        onSubmit={addUserApp}
      />
      <EditUserApp
        UserApp={selectedUserApp}
        visible={!!selectedUserApp}
        onClose={closeModal}
        onEdited={handleEdit}
      />
    </>
  );
};
export default UserAppsPage;
