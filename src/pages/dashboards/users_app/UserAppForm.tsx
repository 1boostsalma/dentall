import React, { useEffect } from 'react';
import { Button, Input } from 'antd';
import { getToken } from '../../../utils/common';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import { hasErrorFactory } from '../../../utils/hasError';
import { IUserApp } from '../../../interfaces/userApps';
//import { AnyARecord } from 'dns';

type Props = {
  UserApp?: IUserApp;
  onSubmit: (UserApp: IUserApp) => void;
  onCancel: () => void;
  submitText?: string;
};

//const defaultSubmitText = 'Ajouter';
const emptyUserApp = {
    nom: '',
    prenom: '',
    email: '',
    password: ''
};

const token = getToken();
const UserAppSchema = Yup.object().shape({
  nom: Yup.string().required(),
  prenom: Yup.string().required(),
  email: Yup.string().required(),
  password: Yup.string().required()
});

function refreshPage(){ 
  //window.location.reload(false); 
}

const UserAppForm = ({
  submitText = '',
  UserApp = emptyUserApp,
  onSubmit,
  onCancel
}: Props) => {
  
  const {
    handleSubmit ,
    handleChange,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    values,
    setValues,
    isValid,
    errors,
    touched,
    resetForm
  } = useFormik<IUserApp>({
    validationSchema: UserAppSchema,
    initialValues: UserApp,
    onSubmit: (form) =>{
      if(submitText=='Ajouter')
    {
      axios.post('https://127.0.0.1:8000/api/user_apps',{
     //"id":values.id,
     "email":values.email,
     "nom": values.nom,
      "prenom": values.prenom,
      "password": values.password,
      "deleted":false
    },
      {
        headers: { 
        'content-type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
      })    
      onSubmit({ ...form });
      resetForm();
     // refreshPage()
     
   }
   if(submitText=='Modifier')
    {
    axios.put(`https://127.0.0.1:8000/api/user_apps/${UserApp.id}`,{
      "email":values.email,
       "nom": values.nom,
        "prenom": values.prenom,
        "password": values.password,
        "deleted":false
     },{
      headers: { 
      'content-type': 'application/json',
      'Authorization': `Bearer ${token}`
    }})
   onSubmit({ ...form });
      resetForm();
  }
}});

  useEffect(() => {
    setValues({ ...values });
  }, [UserApp]);

 
 //const handleStatusSelect = (value) => setFieldValue('status', value);

  const handleCancel = () => {
    resetForm();
    onCancel();
  };
 
  const hasError = hasErrorFactory(touched, errors);

  return (
    <>
      <form onSubmit={handleSubmit}>

        <div className='form-group'>
          <Input
            name='nom'
            placeholder='Nom'
            onBlur={handleBlur}
            onChange={handleChange}
            defaultValue={values.nom}
            className={hasError('nom')}
          />
        </div>

        <div className='form-group'>
          <Input
            defaultValue={values.prenom}
            placeholder='Prenom'
            onBlur={handleBlur}
            name='prenom'
            onChange={handleChange}
            className={hasError('prenom')}
          />
        </div>

        <div className='form-group'>
          <Input
            defaultValue={values.email}
            placeholder='Email'
            name='email'
            type='email'
            onBlur={handleBlur}
            onChange={handleChange}
            className={hasError('email')}
          />
        </div>

        <div className='form-group'>
          <Input
            defaultValue={values.password}
            placeholder='Mot de passe'
            name='password'
            type='password'
            onBlur={handleBlur}
            onChange={handleChange}
            className={hasError('password')}
          />
        </div>
        

        <div className='d-flex justify-content-between buttons-list settings-actions'>
          <Button danger onClick={handleCancel}>
            Annuler
          </Button>
          <Button disabled={!isValid} type='primary' htmlType='submit'>
            {submitText}
          </Button>
        </div>
      </form>
    </>
  );
};

export default UserAppForm;
