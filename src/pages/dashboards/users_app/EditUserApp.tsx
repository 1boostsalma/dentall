import React from 'react';
import { IUserApp } from '../../../interfaces/userApps';
import UserAppForm from './UserAppForm';
import { Modal } from 'antd';

type Props = {
  onEdited: (IUserApp) => void;
  UserApp: IUserApp;
  visible: boolean;
  onClose: () => void;
};

const EditUserApp = ({ UserApp, visible, onClose, onEdited }: Props) => {
  return (
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Modifier</h3>}
    >
      <UserAppForm
        onCancel={onClose}
        onSubmit={onEdited}
        UserApp={UserApp}
        submitText='Modifier'
      />
    </Modal>
  );
};

export default EditUserApp;
