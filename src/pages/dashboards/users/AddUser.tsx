import React from 'react';
import { Modal } from 'antd';

import UserForm from './UserForm';
import { IUser } from '../../../interfaces/users';
type Props = {
  onSubmit: (User: IUser) => void;
  visible: boolean;
  onClose: () => void;
};

const AddUser = ({ visible, onClose, onSubmit }: Props) => {

 
  return (
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Ajouter</h3>}
    >
      <UserForm onCancel={onClose} onSubmit={onSubmit} submitText='Ajouter' />
    </Modal>
  );
};

export default AddUser;
