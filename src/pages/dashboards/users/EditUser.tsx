import React from 'react';
import { IUser } from '../../../interfaces/users';
import UserForm from './UserForm';
import { Modal } from 'antd';

type Props = {
  onEdited: (IUser) => void;
  User: IUser;
  visible: boolean;
  onClose: () => void;
};

const EditUser = ({ User, visible, onClose, onEdited }: Props) => {
  return (
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Modifier User</h3>}
    >
      <UserForm
        onCancel={onClose}
        onSubmit={onEdited}
        User={User}
        submitText='Modifier'
      />
    </Modal>
  );
};

export default EditUser;
