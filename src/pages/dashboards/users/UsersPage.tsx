import React, { useState } from 'react';
import { Button } from 'antd';
import EditUser from './EditUser';
import AddUser from './AddUser';
import UsersTable from '../../../layout/components/users/UsersTable';
import PageAction from '../../../layout/components/page-action/PageAction';
import { useFetchPageData, usePageData } from '../../../hooks/usePage';
import { IUser } from '../../../interfaces/users';
import { IPageData } from '../../../interfaces/page';
import axios from 'axios';
import { getToken } from '../../../utils/common';

const pageData: IPageData = {
  title: 'Utilisateurs/Cabinets',
  fulFilled: false,
  breadcrumbs: [
    {
      title: 'Dashboard',
      route: 'default-dashboard'
    },
    {
      title: 'Utilisateurs/Cabinets'
    }
  ]
};
const UsersPage = () => {
  usePageData(pageData);
  const [Users, setUsers] = useFetchPageData<IUser[]>(
    'https://127.0.0.1:8000/api/users?deleted=false',
    []
    );   
  const [selectedUser, setSelectedUser] = useState(null);
  const [addingModalVisibility, setAddingModalVisibility] = useState(false);
  const handleDelete = (User: IUser) => {
    let token = getToken();
    axios.put(`https://127.0.0.1:8000/api/users/${User.id}`,{
      "deleted": true
    },{
    headers: { 
    'content-type': 'application/json',
    'Authorization': `Bearer ${token}`
  }});
    const newUsers = Users.filter((el) => el !== User);
    setUsers(newUsers);
  };
  const handleEdit = (User: IUser) => {
    const editedUsers = Users.map((el) =>
      el !== selectedUser ? el : User
    );
    setUsers(editedUsers);
    setSelectedUser(null);
  };
  const openAddingModal = () => setAddingModalVisibility(true);
  const addUser = (User: IUser) => {
    setUsers([User, ...Users]);
    setAddingModalVisibility(false);
  };
  const closeAddingModal = () => setAddingModalVisibility(false);
  const openEditModal = (User: IUser) => setSelectedUser(User);
  const closeModal = () => {
    setSelectedUser(null);
  };
  const UsersActions = (User: IUser) => (
    <div className='buttons-list nowrap'>
      <Button onClick={openEditModal.bind({}, User)} shape='circle' type='primary'>
        <span className='icofont icofont-edit-alt' />
      </Button>
      <Button onClick={handleDelete.bind({}, User)} shape='circle' danger>
        <span className='icofont icofont-ui-delete' />
      </Button>
    </div>
  );
  return (
    <>
      <PageAction onClick={openAddingModal} icon='icofont-plus' type={'primary'} />
      <UsersTable data={Users} actions={UsersActions} />
      <AddUser
        onClose={closeAddingModal}
        visible={addingModalVisibility} 
        onSubmit={addUser}
      />
      <EditUser
        User={selectedUser}
        visible={!!selectedUser}
        onClose={closeModal}
        onEdited={handleEdit}
      />
    </>
  );
};
export default UsersPage;
