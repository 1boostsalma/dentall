import React, { useEffect} from 'react';

import { Button, Select, Input } from 'antd';

import { useFetch } from '../../../hooks/useFetch';

import { getToken } from '../../../utils/common';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import { hasErrorFactory } from '../../../utils/hasError';

import { IUser } from '../../../interfaces/users';
//import { AnyARecord } from 'dns';

type Props = {
  User?: IUser;
  onSubmit: (User: IUser) => void;
  onCancel: () => void;
  submitText?: string;
};

const defaultSubmitText = 'Ajouter';
const emptyUser = {
    nom: '',
    prenom: '',
    email: '',
    roles: '',
    cabinet: '',
    telephone: '',
    password: ''
};

const token = getToken();
const UserSchema = Yup.object().shape({
  nom: Yup.string().required(),
  prenom: Yup.string().required(),
  email: Yup.string().required(),
  roles: Yup.string().required(),
  cabinet: Yup.string().required(),
  telephone: Yup.string().required(),
  password: Yup.string().required()
});

function refreshPage(){ 
  window.location.reload(); 
}

const UserForm = ({
  submitText = defaultSubmitText,
  User = emptyUser,
  onSubmit,
  onCancel
}: Props) => {
  const [cabinets] = useFetch<{ value: any }[]>('https://127.0.0.1:8000/api/cabinets?deleted=false', []);


  const {
    handleSubmit ,
    handleChange,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    values,
    setValues,
    isValid,
    errors,
    touched,
    resetForm
  } = useFormik<IUser>({
    validationSchema: UserSchema,
    initialValues: User,
    onSubmit: (form) =>{
      if(submitText=='Ajouter')
    {
      axios.post('https://127.0.0.1:8000/api/users',{
     //"id":values.id,
     "email":values.email,
     "nom": values.nom,
      "prenom": values.prenom,
      "cabinet": values.cabinet,
      "telephone": values.telephone,
      "password": values.password,
      "deleted":false
    },
      {
        headers: { 
        'content-type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
      })    
      onSubmit({ ...form });
      resetForm();
   }
   else  if(submitText=='Modifier')
    {
    axios.put(`https://127.0.0.1:8000/api/users/${User.id}`,{
      "email":values.email,
       "nom": values.nom,
        "prenom": values.prenom,
        "cabinet": values.cabinet,
        "telephone": values.telephone,
        "password": values.password,
        "deleted":false
     },{
      headers: { 
      'content-type': 'application/json',
      'Authorization': `Bearer ${token}`
    }})
   onSubmit({ ...form });
      resetForm();
  }
}});
     
     
  useEffect(() => {
    setValues({ ...values });
  }, [User]);

  const handleCabinetSelect = (cabinet) => {
    setValues({ ...values, cabinet });
  };
 //const handleStatusSelect = (value) => setFieldValue('status', value);

  const handleCancel = () => {
    resetForm();
    onCancel();
  };
 
  const hasError = hasErrorFactory(touched, errors);

  return (
    <>
      <form onSubmit={handleSubmit}>
       

        <div className='form-group'>
          <Input
            name='nom'
            placeholder='Nom'
            onBlur={handleBlur}
            onChange={handleChange}
            defaultValue={values.nom}
            className={hasError('nom')}
          />
        </div>

        <div className='form-group'>
          <Input
            defaultValue={values.prenom}
            placeholder='Prenom'
            onBlur={handleBlur}
            name='prenom'
            onChange={handleChange}
            className={hasError('prenom')}
          />
        </div>

        <div className='form-group'>
          <Input
            defaultValue={values.email}
            placeholder='Email'
            name='email'
            type='email'
            onBlur={handleBlur}
            onChange={handleChange}
            className={hasError('email')}
          />
        </div>

        <div className='form-group'>
          <Input
            defaultValue={values.password}
            placeholder='Mot de passe'
            name='password'
            type='password'
            onBlur={handleBlur}
            onChange={handleChange}
            className={hasError('password')}
          />
        </div>
        <div className='form-group'>
          <Select
            placeholder='Cabinet'
            defaultValue={values.cabinet['nom']}
            onChange={handleCabinetSelect}
            className={hasError('cabinet')}
            onBlur={() => setFieldTouched('cabinet')}
          >
            {               
              cabinets.map((cabinet)=>(
            <Select.Option value={cabinet['@id']}>{cabinet['nom']}</Select.Option>
              ))
            }
          </Select>
        </div>

        <div className='form-group'>
          <Input
            defaultValue={values.roles}
            placeholder='Role'
            name='roles'
            type='roles'
            onBlur={handleBlur}
            onChange={handleChange}
            className={hasError('roles')}
          />
        </div>
          
    
        <div className='form-group'>
          <Input
            type='telephone'
            name='telephone'
            onBlur={handleBlur}
            placeholder='Telephone'
            onChange={handleChange}
            defaultValue={values.telephone}
            className={hasError('telephone')}
          />
        </div>

        <div className='d-flex justify-content-between buttons-list settings-actions'>
          <Button danger onClick={handleCancel}>
              Annuler
          </Button>
          <Button disabled={!isValid} type='primary' htmlType='submit'>
            {submitText}
          </Button>
        </div>
       
      </form>
    </>
  );
};

export default UserForm;
