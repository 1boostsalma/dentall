import React from 'react';

import { Table ,Typography} from 'antd';
import { ColumnProps, TableProps } from 'antd/es/table';

import { IBilling } from '../../../interfaces/patients';
import 'antd/dist/antd.css';
const { Text } = Typography;
   
const columns: ColumnProps<IBilling>[] = [
  {
    key: 'id',
    title: '#',
    sorter: (a, b) => a.id - b.id,
    className: 'text-align-rigth',
    render: ({ id }) => <span className='text-right text-color-200'>{id}</span>
  },
  {
    key: 'patient',
    title: 'Patient',
    sorter: (a, b) => a.patient.localeCompare(b.patient),
    render: ({ patient }) => <span className='text-align-left'>{patient['nom']}{patient['prenom']}</span>
  },
  {
    key: 'date',
    title: 'Date',
    className: 'nowrap',
    sorter: (a, b) => a.date.localeCompare(b.date),
    render: ({ date }) => <span className='text-align-left text-color-200'>{date}</span>
  },

  {
    key: 'charges',
    title: 'Charges',
    render: ({ charges }) => charges
  }

];

type Props = {
  billings: IBilling[];
  pagination?: TableProps<any>['pagination'];
  type?: 'accent' | 'none';
};

const tableClasses = {
  accent: 'accent-header',
  none: ''
};

const BillingTable = ({ billings, pagination = false, type = 'none' }: Props) => {
  const tableClass = tableClasses[type];
 
  return (
    <Table
      className={tableClass}
      pagination={pagination}
      rowKey='id'
      dataSource={billings}
      columns={columns}
      bordered
      summary={pageData => {
        let totalBorrow = 0;
        

        pageData.forEach(({ charges }) => {
          totalBorrow += charges;
          
        });

        return (
          <>
            <Table.Summary.Row>
              <Table.Summary.Cell index={1}>Total</Table.Summary.Cell>
              <Table.Summary.Cell index={2} >
                <Text type="danger">{totalBorrow}</Text>
              </Table.Summary.Cell>
             
            </Table.Summary.Row>
          </>
        );
      }}
    />







  );
};


export default BillingTable;
