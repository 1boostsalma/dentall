
import {removeUserSession} from '../../utils/common';
import { history } from '../../redux/store';



const Logout = () => {
  
    removeUserSession();
    history.push('/');
    window.location.reload(); 

  
};

export default Logout;