import React, {useState} from 'react';
import { Button, Form, Input, Switch } from 'antd';
import { LoginOutlined } from '@ant-design/icons/lib';
import PublicLayout from '../../layout/public/Public';
import { Link } from 'react-router-dom';
import { useForm } from 'antd/es/form/Form';
import { navigateHomeadmin } from '../../utils/naviagate-home';
import axios from 'axios';
import { useFetchPageData } from '../../hooks/usePage';
import { setUserSession } from '../../utils/common';
import { setadminSession } from '../../utils/common';
const { Item } = Form; 
const SignInAdmin = () => {
  const [form] = useForm();
  const [email,setEmail] = useState("");
  const [password,setPassword] = useState("");
  const [error, setError] = useState(null);
   const [admin] = useFetchPageData< []>(
    `https://127.0.0.1:8000/api/user_apps?email=${email}`,
    []
    );
admin.map(cab=>(
 setadminSession(cab['nom'])
  ));
      const login = () => {
        setError(null);
        axios.post('https://127.0.0.1:8000/api/login_admin', { email, password })
        .then(response => {
          setUserSession(response.data.token);
          navigateHomeadmin();
        }).catch(error => {        
          if (error.response.status === 401)
           setError("Informations incorrectes");
          else setError("Quelque chose s'est mal passé. Veuillez réessayer plus tard.");
        });
      }
  return (
    <PublicLayout bgImg={`${window.origin}/content/login-page.jpg`}>
      <h4 className='mt-0 mb-1'>Se Connecter en Tant Qu'Administrateur</h4>
      <p className='text-color-200'>Identifiez vous pour accéder à votre compte</p>
      {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br />
      <Form form={form} layout='vertical' className='mb-4'>
        <Item name='email' rules={[{ required: true, message: <></> }]}>
          <Input placeholder='Email' onChange={(e)=>setEmail(e.target.value)} />
        </Item>
        <Item name='password' rules={[{ required: true, message: <></> }]}>
          <Input placeholder='Password' type='password' onChange={(e)=>setPassword(e.target.value)} />
        </Item>
       
        <Button
          block={false}
          type='primary'
          onClick={login}
          htmlType='submit'
          icon={<LoginOutlined style={{ fontSize: '1.3rem' }} />}
        >
          Login
        </Button>
        
      </Form>
      <br />
      
      <p>
        Se connecter en tant qu'utilisateur <Link to='/'>Se Connecter!</Link>
      </p>
    </PublicLayout>
  );
};
  

export default SignInAdmin;